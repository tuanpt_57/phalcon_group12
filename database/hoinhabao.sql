-- MySQL dump 10.13  Distrib 5.6.21, for Win32 (x86)
--
-- Host: localhost    Database: hoinhabao
-- ------------------------------------------------------
-- Server version	5.6.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chude`
--

DROP TABLE IF EXISTS `chude`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chude` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chude`
--

LOCK TABLES `chude` WRITE;
/*!40000 ALTER TABLE `chude` DISABLE KEYS */;
INSERT INTO `chude` VALUES (2,'Thời sự'),(4,'Pháp luật'),(5,'Thể thao'),(6,'Giáo dục'),(7,'An ninh xã hội'),(8,'Khoa học công nghệ'),(9,'Kinh tế'),(10,'Văn hóa'),(11,'Nghệ thuật');
/*!40000 ALTER TABLE `chude` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `giaithuong`
--

DROP TABLE IF EXISTS `giaithuong`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `giaithuong` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loaigiaithuong_id` int(11) NOT NULL,
  `hoivien_id` int(11) NOT NULL,
  `date_rewarded` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_giaithuong_hoivien` (`hoivien_id`),
  KEY `fk_giaithuong_loaigiaithuong` (`loaigiaithuong_id`),
  CONSTRAINT `fk_giaithuong_hoivien` FOREIGN KEY (`hoivien_id`) REFERENCES `hoivien` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_giaithuong_loaigiaithuong` FOREIGN KEY (`loaigiaithuong_id`) REFERENCES `loaigiaithuong` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `giaithuong`
--

LOCK TABLES `giaithuong` WRITE;
/*!40000 ALTER TABLE `giaithuong` DISABLE KEYS */;
INSERT INTO `giaithuong` VALUES (14,2,14,'2014-08-08'),(17,1,1,'2015-04-21'),(19,7,13,'2015-04-21'),(20,6,15,'2014-09-08'),(21,4,19,'2015-04-05'),(22,4,14,'2015-04-08'),(23,5,14,'2015-05-16');
/*!40000 ALTER TABLE `giaithuong` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hoivien`
--

DROP TABLE IF EXISTS `hoivien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hoivien` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date NOT NULL,
  `gender` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_card` int(20) NOT NULL,
  `biography` longtext COLLATE utf8_unicode_ci,
  `role_id` int(1) NOT NULL DEFAULT '3',
  `is_active` int(1) NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ui_template` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_hoivien_role` (`role_id`),
  CONSTRAINT `fk_hoivien_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hoivien`
--

LOCK TABLES `hoivien` WRITE;
/*!40000 ALTER TABLE `hoivien` DISABLE KEYS */;
INSERT INTO `hoivien` VALUES (1,'admin','admin','Administrator','Admin','1994-06-21','male','assets/uploads/avatar/admin.png','Thanh Hóa','tuanpt216@gmail.com',193317918,'Người có quyền lực cao nhất',1,1,'2015-04-25 08:10:55',1),(13,'mod','mod','Mod','Mod','2015-04-17','male','assets/uploads/avatar/mod.jpeg','TH City','abc@local.web',1331,'The quick brown fox jumped over the lazy black dog',2,1,'2015-04-25 08:14:14',1),(14,'member','member','Member','Member','1980-03-05','male','assets/uploads/avatar/default_male.jpg','Vương quốc Anh','abc@local.web',133132,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',3,1,'2015-05-16 17:57:20',1),(15,'tester','tester','Tester','Tester','2015-04-21','female','assets/uploads/avatar/tester.jpeg','Hà Nội','abc@local.web',2147483647,'Tester vs. Developer',3,0,'2015-04-27 04:59:14',1),(17,'user','user','User','User','2015-04-06','male','assets/uploads/avatar/default_male.jpg','Hà Nội','abc@local.web',12810390,'User with avatar',3,1,'2015-04-27 04:52:45',1),(19,'userf','userf','Userf','Userf','2015-04-25','female','assets/uploads/avatar/default_female.jpg','Hà Nội','abc@local.web',12810390,'userf',3,1,'2015-04-25 08:23:27',1),(20,'abv','abv','abv','abv','2015-04-26','male','assets/uploads/avatar/default_male.jpg','Hà Nội','abc@local.web',12810390,'akl',3,0,'2015-04-26 09:35:53',1),(24,'c','c','c','cc','2015-04-26','male','assets/uploads/avatar/default_male.jpg','c','abc@local.websaas',12567890,'ds',3,0,'2015-04-26 09:56:19',1),(25,'Akat','akat','Akat','Akat','2015-05-16','male','assets/uploads/avatar/default_male.jpg','Vương quốc Anh','demo@phalconphp.com',13312,'Biography',3,0,'2015-05-16 15:35:28',1);
/*!40000 ALTER TABLE `hoivien` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loaigiaithuong`
--

DROP TABLE IF EXISTS `loaigiaithuong`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loaigiaithuong` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `info` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loaigiaithuong`
--

LOCK TABLES `loaigiaithuong` WRITE;
/*!40000 ALTER TABLE `loaigiaithuong` DISABLE KEYS */;
INSERT INTO `loaigiaithuong` VALUES (1,'Giải thưởng Hồ Chí Minh','Giải thưởng Hồ Chí Minh là một giải thưởng của Nhà nước Cộng hòa xã hội chủ nghĩa Việt Nam tặng những công trình nghiên cứu khoa học, kỹ thuật, những công trình giáo dục và văn học, nghệ thuật đặc biệt xuất sắc, có giá trị rất cao về khoa học, văn học, nghệ thuật, về nội dung tư tưởng, có tác dụng lớn phục vụ sự nghiệp cách mạng, có ảnh hưởng rộng lớn và lâu dài trong đời sống nhân dân, góp phần quan trọng vào sự nghiệp phát triển nền kinh tế quốc dân, khoa học, kỹ thuật, văn học, nghệ thuật. Đây được xem là giải thưởng danh giá nhất hiện nay tại Việt Nam.\r\n\r\nGiải thưởng Hồ Chí Minh được xét và công bố năm năm một lần vào dịp Quốc khánh 2-9.'),(2,'Giải thưởng Báo chí Lao động – Việc làm',' Giải thưởng hằng năm  do Học viện Báo chí và Tuyên truyền, Ban Tuyên giáo Trung ương và Tổ chức Lao động Quốc tế đồng tổ chức, với tổng giá trị giải thưởng lên tới 100 triệu đồng.'),(3,'Giải thưởng báo chí Huỳnh Thúc Kháng','Giải thưởng dành cho các tác phẩm báo chí có chủ đề về con người và quê hương Quảng Nam dành cho tác phẩm dự thi ở các thể loại: báo viết, báo nói, báo hình và ảnh báo chí được đăng tải trên các phương tiện thông tin đại chúng trong cả nước có thời hạn một năm tính từ ngày 1/5 năm trước đến ngày 1/5 năm sau, không phân biệt tác giả là người làm báo trong hay ngoài tỉnh.'),(4,'Giải thưởng báo chí môi trường châu Á','Phần thưởng cao quý dành cho các nhà báo có thành tích xuất sắc trong lĩnh vực môi trường do Ủy ban Môi trường Singapore (SEC) tổ chức với sự hỗ trợ của nhiều tổ chức truyền thông về môi trường trên toàn châu Á.'),(5,'Giải báo chí về an toàn giao thông','Giải thưởng do Bộ Thông tin và Truyền thông, Ủy ban An toàn giao thông Quốc gia phối hợp tổ chức, trao cho các tác phẩm báo chí về chủ đề an toàn giao thông nhằm nâng cao hơn nữa ý thức tham gia giao thông của mọi người, góp phần kiềm chế ùn tắc, tai nạn giao thông.'),(6,'Giải báo chí Quốc gia','Giải thưởng được tổ chức hằng năm nhằm động viên, cổ vũ phong trào thi đua lao động sáng tạo của đội ngũ người làm báo, phát hiện, bồi dưỡng những tài năng báo chí trong xã hội.'),(7,'Giải báo chí khoa học công nghệ','Giải thưởng ghi nhận và tôn vinh những tác giả có tác phẩm báo chí xuất sắc,  tạo động lực cho các phóng viên, cơ quan báo chí tích cực tham gia tuyên truyền về khoa học và công nghệ, đưa nhanh kết quả nghiên cứu vào cuộc sống, sản xuất kinh doanh.');
/*!40000 ALTER TABLE `loaigiaithuong` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info` text COLLATE utf8_unicode_ci,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
INSERT INTO `logs` VALUES (1,'User Mod thêm tác phẩm Những chuyến đi bão','2015-04-26 09:04:49'),(2,'User Mod xóa tác phẩm Những chuyến đi bão','2015-04-26 09:07:50'),(3,'User Mod thêm tác phẩm Biến đổi khí hậu ở Việt Nam','2015-04-26 09:09:06'),(4,'User Administrator cập nhật tác phẩm Ngày và đêm','2015-04-26 09:46:30'),(5,'User Administrator thêm tác phẩm Lý Sơn - Bảo tàng Hoàng Sa giữa biển','2015-04-26 09:49:59'),(6,'User Administrator thêm tác phẩm Biến khí thải CO2 thành pin năng lượng','2015-04-26 09:56:06'),(7,'User Administrator cập nhật tác phẩm Biến đổi khí hậu ở Việt Nam','2015-04-26 09:56:57'),(8,'User Administrator cập nhật tác phẩm Công nhân về hưu thành… người nghèo','2015-04-26 09:57:15'),(9,'User Administrator cập nhật tác phẩm Lý Sơn - Bảo tàng Hoàng Sa giữa biển','2015-04-26 10:16:02'),(10,'User admin thêm giải thưởng ','2015-04-26 10:22:16'),(11,'User admin thêm giải thưởng ID 21','2015-04-26 10:29:03'),(12,'User Member thêm tác phẩm Nhật ký trên con đường huyền thoại','2015-04-26 17:36:00'),(13,'User Member thêm tác phẩm Nhật ký trên con đường huyền thoại','2015-04-26 17:36:23'),(14,'User Administrator xóa tác phẩm Nhật ký trên con đường huyền thoại','2015-04-26 17:37:20'),(15,'User Administrator thêm tác phẩm abc','2015-04-26 17:56:02'),(16,'User Administrator xóa tác phẩm abc','2015-04-26 17:56:11'),(17,'User Administrator cập nhật tác phẩm Thanh niên miền Tây bứt phá làm giàu','2015-04-27 03:51:31'),(18,'User Administrator cập nhật tác phẩm  Tôi luôn thấy mắc nợ với nông dân','2015-04-27 03:52:20'),(19,'User Administrator cập nhật tác phẩm Nhật ký trên con đường huyền thoại','2015-04-27 03:53:25'),(20,'User Administrator cập nhật tác phẩm Biến khí thải CO2 thành pin năng lượng','2015-04-27 03:54:02'),(21,'User Administrator cập nhật tác phẩm Lý Sơn - Bảo tàng Hoàng Sa giữa biển','2015-04-27 03:54:20'),(22,'User Administrator cập nhật tác phẩm Bếp rơm không khói','2015-04-27 03:55:04'),(23,'User Administrator cập nhật tác phẩm Đau xót nạn bóc lột lao động nhà quê','2015-04-27 03:55:23'),(24,'User Administrator cập nhật tác phẩm Công nhân về hưu thành… người nghèo','2015-04-27 04:02:12'),(25,'User Administrator cập nhật tác phẩm Những ông chủ “lời ăn lỗ… chạy','2015-04-27 04:03:40'),(26,'User Administrator thêm tác phẩm abbb','2015-04-27 06:08:09'),(27,'User Administrator thêm tác phẩm jkasl','2015-04-27 10:37:53'),(28,'User Administrator cập nhật tác phẩm Biến khí thải CO2 thành pin năng lượng','2015-04-27 10:54:47'),(29,'User Administrator cập nhật tác phẩm Bếp rơm không khói','2015-04-27 10:55:09'),(30,'User Administrator cập nhật tác phẩm Lý Sơn - Bảo tàng Hoàng Sa giữa biển','2015-04-27 10:55:24'),(31,'User Administrator cập nhật tác phẩm Thanh niên miền Tây bứt phá làm giàu','2015-04-27 10:55:32'),(32,'User Administrator cập nhật tác phẩm  Tôi luôn thấy mắc nợ với nông dân','2015-04-27 10:55:46'),(33,'User Administrator cập nhật tác phẩm PGS. TS Võ Công Thành thoát hiểm cho cây lúa','2015-04-27 10:55:52'),(34,'User Administrator cập nhật tác phẩm Những ông chủ “lời ăn lỗ… chạy','2015-04-27 10:56:06'),(35,'User Administrator cập nhật tác phẩm Công nhân về hưu thành… người nghèo','2015-04-27 10:56:24'),(36,'User Administrator cập nhật tác phẩm Biến đổi khí hậu ở Việt Nam','2015-04-27 10:56:31'),(37,'User Administrator cập nhật tác phẩm Ngày và đêm','2015-04-27 10:56:43'),(38,'User Administrator cập nhật tác phẩm PGS. TS Võ Công Thành thoát hiểm cho cây lúa','2015-04-27 13:20:39'),(39,'User admin thêm giải thưởng ID 22','2015-04-27 13:28:25'),(40,'User Administrator xóa các tác phẩm ','2015-05-11 21:31:30'),(41,'User Member thêm tác phẩm Abss','2015-05-16 23:10:20'),(42,'User Member xóa tác phẩm Nhật ký trên con đường huyền thoại','2015-05-16 23:15:49'),(43,'User Administrator cập nhật tác phẩm Abss','2015-05-16 23:17:31'),(44,'User Member xóa tác phẩm Abss','2015-05-16 23:17:46'),(45,'User Administrator thêm tác phẩm sa','2015-05-16 23:18:12'),(46,'User Administrator thêm tác phẩm sdk','2015-05-16 23:22:14'),(47,'User Administrator xóa các tác phẩm ','2015-05-16 23:22:30'),(48,'User member thêm giải thưởng ID 23','2015-05-16 23:36:21'),(49,'User member sửa giải thưởng 14','2015-05-16 23:37:11');
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(1) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `info` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Admin','Người quản trị có quyền cao nhất trong hệ thống, có quyền thêm sửa xóa các Moderator, Member, tác phẩm, giải thưởng...'),(2,'Moderator','Người quản trị thứ hai trong hệ thống, có quyền thêm sửa xóa các Member, tác phẩm, giải thưởng'),(3,'Member','Hội viên hội nhà báo có quyền thêm, sửa, xóa tác phẩm, giải thưởng'),(4,'Guest','Khách truy cập vào hệ thống, không có tài khoản.');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tacpham`
--

DROP TABLE IF EXISTS `tacpham`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tacpham` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hoivien_id` int(11) NOT NULL,
  `chude_id` int(11) NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_composed` date NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `added_at` datetime DEFAULT NULL,
  `info` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `fk_tacpham_hoivien` (`hoivien_id`),
  KEY `fk_tacpham_chude` (`chude_id`),
  CONSTRAINT `fk_tacpham_chude` FOREIGN KEY (`chude_id`) REFERENCES `chude` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tacpham_hoivien` FOREIGN KEY (`hoivien_id`) REFERENCES `hoivien` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tacpham`
--

LOCK TABLES `tacpham` WRITE;
/*!40000 ALTER TABLE `tacpham` DISABLE KEYS */;
INSERT INTO `tacpham` VALUES (7,'Thanh niên miền Tây bứt phá làm giàu',14,9,'Thanh niên, Đời sống, Làm giàu','2015-04-24','http://www.baocantho.com.vn/?mod=detnews&catid=184&p=&id=156031','2015-04-27 10:55:32','Những thanh niên lập nghiệp thành công cho rằng, để ngày càng có nhiều tỉ phú trẻ, ngoài sự năng động, nhạy bén và sự nỗ lực của bản thân, thanh niên cần đẩy mạnh liên kết hỗ trợ nhau làm giàu, tập trung phát triển các mô hình kinh tế tập thể. Nhưng thực trạng thiếu vốn, trình độ nắm bắt, ứng dụng khoa học kỹ thuật vào sản xuất của nhiều thanh niên còn hạn chế, những khó khăn trong việc triển khai các mô hình hợp tác xã (HTX), tổ hợp tác (THT) thanh niên… đã và đang là rào cản trên bước đường lập nghiệp của nhiều người trẻ. Nhiều thanh niên rất cần sự trợ lực kịp thời của các cấp, các ngành, các đoàn thể để vươn lên làm giàu chính đáng. '),(8,' Tôi luôn thấy mắc nợ với nông dân',14,10,'Đời sống, Nông thôn','2015-04-24','http://tuoitre.vn/tin/tuoi-tre-cuoi-tuan/20131118/toi-luon-thay-minh-mac-no-voi-nong-dan/580354.html','2015-04-27 10:55:45','TTCT - “Tôi luôn cảm thấy mình “mắc nợ” với nông dân và tâm niệm làm sao để hạn chế tối đa những thiệt hại của dân mình do thiên tai” - giáo sư Trần Đình Hòa (phó viện trưởng Viện Khoa học thủy lợi Việt Nam), vị giáo sư trẻ nhất năm 2013 vừa được đặc cách công nhận, người cũng xuất thân từ gia đình nông dân - chia sẻ với TTCT.'),(9,'PGS. TS Võ Công Thành thoát hiểm cho cây lúa',14,8,'Đời sống, Nông thôn','2015-04-24','http://www.thanhnien.com.vn/doi-song/pgsts-vo-cong-thanh-thoat-hiem-cho-cay-lua-42969.html','2015-04-27 13:20:38','abc'),(10,'Công nhân về hưu thành… người nghèo',15,7,'Đời sống, Công nhân','2015-04-24','http://www.sggp.org.vn/phongsudieutra/2014/7/354510/','2015-04-27 10:56:24','Hết tuổi lao động, bạn sẽ sống như thế nào với mức lương hưu chỉ trên 1 triệu đồng mỗi tháng, tương đương khoảng 35.000 đồng/ngày? Hơn 20 năm kể từ thời điểm có các doanh nghiệp khu vực ngoài nhà nước đi vào hoạt động (đầu những năm 1990) đến nay, một bộ phận công nhân đã đến tuổi hưu trí. Sau hàng chục năm lao động vất vả, giờ đây, theo quy định hiện hành, nhiều công nhân được hưởng lương hưu với mức tương đương… chuẩn nghèo của TPHCM. Thời gian tới, khi công nhân khu vực ngoài nhà nước về hưu ồ ạt, TPHCM cũng như cả nước sẽ phải đối mặt với những thách thức như thế nào trong việc đảm bảo an sinh xã hội? Giải pháp nào để giúp một bộ phận công nhân hết tuổi lao động bớt khó khăn? Phòng ngừa tình trạng nghèo khi về hưu như thế nào?\r\n'),(11,'Đau xót nạn bóc lột lao động nhà quê',14,2,'Đời sống, Nông thôn','2015-04-24','http://tuoitre.vn/tin/chinh-tri-xa-hoi/20140714/dau-xot-nan-boc-lot-lao-dong-nha-que/621733.html','2015-04-27 03:55:23','TT - Hai phóng viên báo Tuổi Trẻ đã hóa thân thành những người lao động nhà quê thâm nhập các đường dây môi giới lao động để vạch trần thủ đoạn kiếm tiền trên thân xác người nghèo khổ.'),(12,'Những ông chủ “lời ăn lỗ… chạy',14,9,'Đời sống, Công nhân','2015-04-24','http://dantri.com.vn/kinh-doanh/nhung-ong-chu-loi-an-lo-chay-bai-1-983140.htm','2015-04-27 10:56:06','Theo nhận định từ cơ quan chức năng, 9 tháng đầu năm, trên địa bàn TPHCM và một số tỉnh phía Nam lại rộ lên “phong trào” chủ doanh nghiệp (DN) mất tích khỏi trụ sở khiến hàng trăm công nhân rơi vào cảnh khốn đốn…'),(13,'Bếp rơm không khói',13,8,'Đời sống, Nông thôn, Công nghệ','2015-04-24','http://vietnamnet.vn/vn/khoa-hoc/106459/bep-hoa-khi-sach-moi-truong--made-in-vietnam-.html','2015-04-27 10:55:09','Loại bếp mới do nhà sáng chế không chuyên Bùi Trọng Tuấn ở Phú Thọ chế tạo được coi là một giải pháp mới vừa tiết kiệm vừa góp phần làm sạch môi trường.'),(14,'Ngày và đêm',17,11,'Cuộc sống,Thiên nhiên,Khoa học','2015-04-23','http://abc.com','2015-04-27 10:56:43',''),(20,'Biến đổi khí hậu ở Việt Nam',15,8,'Môi trường,Đời sống,Khí hậu','2015-04-20','http://abc.com','2015-04-27 10:56:31',''),(21,'Lý Sơn - Bảo tàng Hoàng Sa giữa biển',13,7,'Quân sự,Đất nước,Hoàng Sa,Biển đảo','2015-04-15','http://www.qdnd.vn/qdndsite/vi-vn/61/43/phong-su/ly-son-bao-tang-hoang-sa-giua-bien-ky-1/79828.html','2015-04-27 10:55:24','Huyện Lý Sơn, tên dân gian gọi là Cù Lao Ré, nằm về phía đông bắc tỉnh Quảng Ngãi, cách đất liền 15 hải lý, có diện tích gần 10km2, gồm hai đảo: Đảo lớn (Cù lao Ré) và đảo nhỏ (Cù lao Bờ Bãi). Ở phía đông Cù lao Ré còn có hòn Mù Cu, là một bãi đá nhô cao trên mặt biển. Đến Lý Sơn, chúng ta không chỉ được khám phá nền văn hóa Sa Huỳnh, Chăm-pa đã từng có mặt  từ mấy chục thế kỷ trước mà còn tìm hiểu một bảo tàng về Hoàng Sa, với những di tích, hiện vật quý - những bằng chứng khẳng định chân lý duy nhất đúng: Quần đảo Hoàng Sa là máu thịt, là chủ quyền lãnh thổ thiêng liêng của Tổ quốc Việt Nam…'),(22,'Biến khí thải CO2 thành pin năng lượng',1,8,'Khoa học,Môi trường,Năng lượng','2015-04-24','http://vnexpress.net/tin-tuc/khoa-hoc/bien-khi-thai-co2-thanh-pin-nang-luong-3204887.html','2015-04-27 10:54:47','Các nhà khoa học Mỹ đề xuất phương pháp pháp biến CO2 thành những &#34;cục pin&#34; khổng lồ lưu trữ năng lượng dư thừa dưới đất, giải quyết tình trạng lãng phí và giảm khí thải ô nhiễm. ');
/*!40000 ALTER TABLE `tacpham` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-17  6:59:40
