<?php

class Giaithuong extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $loaigiaithuong_id;

    /**
     *
     * @var integer
     */
    protected $hoivien_id;

    /**
     *
     * @var string
     */
    protected $date_rewarded;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field loaigiaithuong_id
     *
     * @param integer $loaigiaithuong_id
     * @return $this
     */
    public function setLoaigiaithuongId($loaigiaithuong_id)
    {
        $this->loaigiaithuong_id = $loaigiaithuong_id;

        return $this;
    }

    /**
     * Method to set the value of field hoivien_id
     *
     * @param integer $hoivien_id
     * @return $this
     */
    public function setHoivienId($hoivien_id)
    {
        $this->hoivien_id = $hoivien_id;

        return $this;
    }

    /**
     * Method to set the value of field date_rewarded
     *
     * @param string $date_rewarded
     * @return $this
     */
    public function setDateRewarded($date_rewarded)
    {
        $this->date_rewarded = $date_rewarded;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field loaigiaithuong_id
     *
     * @return integer
     */
    public function getLoaigiaithuongId()
    {
        return $this->loaigiaithuong_id;
    }

    /**
     * Returns the value of field hoivien_id
     *
     * @return integer
     */
    public function getHoivienId()
    {
        return $this->hoivien_id;
    }

    /**
     * Returns the value of field date_rewarded
     *
     * @return string
     */
    public function getDateRewarded()
    {
        return $this->date_rewarded;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('hoivien_id', 'Hoivien', 'id', array('alias' => 'Hoivien'));
        $this->belongsTo('loaigiaithuong_id', 'Loaigiaithuong', 'id', array('alias' => 'Loaigiaithuong'));
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'loaigiaithuong_id' => 'loaigiaithuong_id', 
            'hoivien_id' => 'hoivien_id', 
            'date_rewarded' => 'date_rewarded'
        );
    }

}
