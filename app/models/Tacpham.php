<?php

class Tacpham extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var integer
     */
    protected $hoivien_id;

    /**
     *
     * @var integer
     */
    protected $chude_id;

    /**
     *
     * @var string
     */
    protected $keywords;

    /**
     *
     * @var string
     */
    protected $date_composed;

    /**
     *
     * @var string
     */
    protected $link;

    /**
     *
     * @var string
     */
    protected $added_at;

    /**
     *
     * @var string
     */
    protected $info;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field hoivien_id
     *
     * @param integer $hoivien_id
     * @return $this
     */
    public function setHoivienId($hoivien_id)
    {
        $this->hoivien_id = $hoivien_id;

        return $this;
    }

    /**
     * Method to set the value of field chude_id
     *
     * @param integer $chude_id
     * @return $this
     */
    public function setChudeId($chude_id)
    {
        $this->chude_id = $chude_id;

        return $this;
    }

    /**
     * Method to set the value of field keywords
     *
     * @param string $keywords
     * @return $this
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Method to set the value of field date_composed
     *
     * @param string $date_composed
     * @return $this
     */
    public function setDateComposed($date_composed)
    {
        $this->date_composed = $date_composed;

        return $this;
    }

    /**
     * Method to set the value of field link
     *
     * @param string $link
     * @return $this
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Method to set the value of field added_at
     *
     * @param string $added_at
     * @return $this
     */
    public function setAddedAt($added_at)
    {
        $this->added_at = $added_at;

        return $this;
    }

    /**
     * Method to set the value of field info
     *
     * @param string $info
     * @return $this
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field hoivien_id
     *
     * @return integer
     */
    public function getHoivienId()
    {
        return $this->hoivien_id;
    }

    /**
     * Returns the value of field chude_id
     *
     * @return integer
     */
    public function getChudeId()
    {
        return $this->chude_id;
    }

    /**
     * Returns the value of field keywords
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Returns the value of field date_composed
     *
     * @return string
     */
    public function getDateComposed()
    {
        return $this->date_composed;
    }

    /**
     * Returns the value of field link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Returns the value of field added_at
     *
     * @return string
     */
    public function getAddedAt()
    {
        return $this->added_at;
    }

    /**
     * Returns the value of field info
     *
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('chude_id', 'Chude', 'id', array('alias' => 'Chude'));
        $this->belongsTo('hoivien_id', 'Hoivien', 'id', array('alias' => 'Hoivien'));
    }

    /**
     * @return Tacpham[]
     */
    public static function find($parameters = array())
    {
        return parent::find($parameters);
    }

    /**
     * @return Tacpham
     */
    public static function findFirst($parameters = array())
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'name' => 'name', 
            'hoivien_id' => 'hoivien_id', 
            'chude_id' => 'chude_id', 
            'keywords' => 'keywords', 
            'date_composed' => 'date_composed', 
            'link' => 'link', 
            'added_at' => 'added_at', 
            'info' => 'info'
        );
    }

}
