<?php

use Phalcon\Mvc\User\Component;

/**
 * Xác thực, định danh người dùng
 */
class Auth extends Component
{

    /**
     * Kiểm tra hồ sơ người dùng (username, password)
     *
     * @param array $credentials
     * @return boolean
     */
    public function check($credentials)
    {

        // Kiểm tra tài khoản có tồn tại
        $user = Hoivien::findFirstByUsername($credentials['username']);
        if ($user == false) {
            $this->flash->error('Sai tên đăng nhập hoặc mật khẩu!');
        } else {
            // Kiểm tra the password
            if ($credentials['password'] !== $user->password) {
                $this->flash->error('Sai tên đăng nhập hoặc mật khẩu!');
                return false;
            }

            // Kiểm tra tài khoản đã được kích hoạt chưa
            if ($user->getIsActive() != '1') {
                $this->flash->error('User chưa được kích hoạt!');
                return false;
            }

            $this->session->set('vja-identity', array(
                'id' => $user->id,
                'username' => $user->username,
                'name' => $user->name,
                'profile' => $user->roleId,
            ));

            $this->session->theme = $user->getUiTemplate();

            // Kiểm tra xem người dùng có muốn ghi nhớ đăng nhập
            if (isset($credentials['remember'])) {
                // $this->cookies->set('vja-remember-login', $user->id, time() + 15 * 86400);
                $userAgent = $this->request->getUserAgent();
                $token = md5($user->username . $user->password . $userAgent);

                $expire = time() + 86400 * 8;
                $this->cookies->set('vja_remember_id', $user->id, $expire);
                $this->cookies->set('vja_remember_token', $token, $expire);
            }

            return true;
        }
    }

    /**
     * Kiểm tra sự tồn tại của cookies
     *
     * @return boolean
     */
    public function hasRememberMe()
    {
        return $this->cookies->has('vja_remember_id');
    }

    /**
     * Đăng nhập bằng cookies
     *
     * @return Phalcon\Http\Response
     */
    public function loginWithRememberMe()
    {
        $userId = $this->cookies->get('vja_remember_id')->getValue();
        $cookieToken = $this->cookies->get('vja_remember_token')->getValue();

        $user = Hoivien::findFirstById($userId);
        if ($user) {

            $userAgent = $this->request->getUserAgent();
            $token = md5($user->username . $user->password . $userAgent);

            if ($cookieToken == $token) {
                // Kiểm tra tài khoản đã được kích hoạt chưa
                if ($user->getIsActive() != '1') {
                    $this->flash->error('User chưa được kích hoạt!');
                    return false;
                }

                // Register identity
                $this->session->set('vja-identity', array(
                    'id' => $user->id,
                    'username' => $user->username,
                    'name' => $user->name,
                    'profile' => $user->roleId,
                ));

                return $this->response->redirect('.');
            }
        }

        $this->cookies->get('vja_remember_id')->delete();
        $this->cookies->get('vja_remember_token')->delete();

        return $this->response->redirect('profile/login');
    }

    /**
     * Trả về định danh hiện tại
     *
     * @return array
     */
    public function getIdentity()
    {
        return $this->session->get('vja-identity');
    }

    /**
     * Trả về user với định danh hiện tại
     *
     * @return Hoivien
     */
    public function getUser()
    {
        $identity = $this->session->get('vja-identity');
        if (isset($identity['id'])) {

            $user = Hoivien::findFirstById($identity['id']);
            if ($user == false) {
                throw new Exception('The user does not exist');
            }

            return $user;
        }

        return false;
    }

    /**
     * Xóa session, cookies
     */
    public function remove()
    {
        if ($this->cookies->has('vja_remember_id')) {
            $this->cookies->get('vja_remember_id')->delete();
        }
        if ($this->cookies->has('vja_remember_token')) {
            $this->cookies->get('vja_remember_token')->delete();
        }

        $this->session->remove('vja-identity');
    }
}
