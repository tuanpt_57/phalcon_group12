<?php

use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Phalcon\Tag as Tag;

class MyPaginatorModel extends PaginatorModel
{

    public function renderBootstrap()
    {
        $link = isset($this->_config['link']) ? rtrim($this->_config['link'], "/") : "";
        $data = $this->getPaginate();
        echo "<ul class='pagination'>";
        if ($data->first == $data->current) {
            echo "<li class='disabled'><a href='#'><i class='fa fa-step-backward'></i></a></li>";
        } else {
            echo "<li>" . Tag::linkTo($link . "/" . $data->first, "<i class='fa fa-fast-backward'></i>") . "</li>";
            echo "<li>" . Tag::linkTo($link . "/" . $data->before, "<i class='fa fa-step-backward'></i>") . "</li>";
        }
        foreach (range(1, $data->total_pages) as $page) {
            if ($page == $data->current) {
                echo "<li class='active'><a href='#'>$page <span class='sr-only'>(current)</span></a></li>";
            } else {
                echo "<li>" . Tag::linkTo($link . "/" . $page, "$page <span class='sr-only'>(current)</span>") . "</li>";
            }
        }
        if ($data->last == $data->current) {
            echo "<li class='disabled'><a href='#'><i class='fa fa-step-forward'></i></a></li>";
        } else {
            echo "<li>" . Tag::linkTo($link . "/" . ($data->current + 1), "<i class='fa fa-step-forward'></i>") . "</li>";
            echo "<li>" . Tag::linkTo($link . "/" . $data->last, "<i class='fa fa-fast-forward'></i>") . "</li>";
        }
        echo "</ul>";
    }
}
