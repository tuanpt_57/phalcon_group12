<?php
/*
 * Định nghĩa các route bổ sung cho ứng dụng
 */
$router = new Phalcon\Mvc\Router();

$router->add('/u/{name}', array(
    'controller' => 'profile',
    'action' => 'index'
));

$router->add('/profile/login', array(
    'controller' => 'profile',
    'action' => 'login'
));

$router->add('/login', array(
    'controller' => 'profile',
    'action' => 'login'
));

$router->add('/profile/logout', array(
    'controller' => 'profile',
    'action' => 'logout'
));

$router->add('/search/{name}', array(
    'controller' => 'index',
    'action' => 'search'
));

$router->add('/tac-pham/:action/:params', array(
    'controller' => 'tacpham',
    'action' => 1,
    'params' => 2
));

return $router;
