<?php

use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Session as FlashSession;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Http\Response\Cookies;
use Phalcon\Crypt;
use Phalcon\Logger\Adapter\File as FileAdapter;
use Phalcon\Logger\Formatter\Line as LineFormatter;

/**
 * FactoryDefault DI tự động đăng ký tất cả các thành phần của framework
 */
$di = new FactoryDefault();

/**
 * Thành phần quản lý sự kiện
 */
$di->set('dispatcher', function() use ($di) {
    $eventsManager = new EventsManager;

    /**
     * Kiểm tra user có quyền truy cập controller/action bằng SecurityPlugin
     */
    $eventsManager->attach('dispatch:beforeDispatch', new SecurityPlugin);

    $dispatcher = new Dispatcher;
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
});

/**
 * Load các cài đặt route trong file routes.php
 */
$di->set('router', function () {
    return require __DIR__ . '/routes.php';
});

/**
 * Thành phần url giúp tạo các url từ đường dẫn tương đối,
 * dựa trên đường dẫn cơ sở baseUri được khai báo trong file config
 */
$di->set('url', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
}, true);

/**
 * Thành phần view, sử dùng Volt Template Engine
 */
$di->set('view', function () use ($config) {

    $view = new View();

    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines(array(
        '.volt' => function ($view, $di) use ($config) {

            $volt = new VoltEngine($view, $di);

            $volt->setOptions(array(
                'compiledPath' => $config->application->cacheDir,
                'compiledSeparator' => '_',
                'compileAlways' => true
            ));

            return $volt;
        },
        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
    ));

    return $view;
}, true);

/**
 * Kết nối CSDL
 */
$di->set('db', function () use ($config) {
    return new DbAdapter(array(
        'host' => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname' => $config->database->dbname,
        "charset" => $config->database->charset
    ));
});

/**
 * Adapter quản lý metadata của các model
 */
$di->set('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Session
 */
$di->set('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});


/**
 * Thành phần quản lý các thông điệp.
 * Cấu hình các loại thông báo tương ứng với các lớp CSS
 */
$di->set('flash', function () {
    return new FlashSession(array(
        'error' => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice' => 'alert alert-info'
    ));
});

/**
 * Thành phần quản lý việc xác thực, định danh người dùng
 */
$di->set('auth', function () {
    return new Auth();
});

/**
 * Thành phần mã hóa, dùng cho mã hóa cookies
*/
$di->set('crypt', function () use ($config) {
    $crypt = new Phalcon\Crypt();
    $crypt->setKey($config->application->encryptKey);
    return $crypt;
});

/**
 * Thành phần quản lý cookies
*/
$di->set('cookies', function() {
    $cookies = new Cookies();
    return $cookies;
});

