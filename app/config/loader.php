<?php

$loader = new \Phalcon\Loader();

/**
 * Load các class trong các thư mục đã được khai báo trong file config
 */
$loader->registerDirs(
    array(
        $config->application->controllersDir,
        $config->application->modelsDir,
        $config->application->pluginsDir,
        $config->application->libraryDir,
        $config->application->formsDir
    )
);

$loader->register();
