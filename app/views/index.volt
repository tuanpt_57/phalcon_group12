<!-- views/index.volt -->
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Grourp 12 - UET">
    <link rel="icon" href="{{url('assets/img/favicon.png')}}">

    {{ get_title() }}

    <!-- Bootstrap Core CSS -->
    {% if this.session.theme == 3 %}
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/second-style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/main-style.css')}}">
    {{ stylesheet_link("assets/bootstrap/css/bootstrap-paper.min.css") }}
    {% elseif this.session.theme == 2 %}
    {{ stylesheet_link("assets/bootstrap/css/bootstrap.min.css") }}
    {% else %}
    {{ stylesheet_link("assets/bootstrap/css/bootstrap-readable.min.css") }}
    {% endif %}

    {{ stylesheet_link("assets/bootstrap-datepicker/css/bootstrap-datepicker3.min.css") }}

    <!-- Custom CSS -->

    <!-- Custom Fonts -->
    {{ stylesheet_link("assets/font-awesome/css/font-awesome.min.css") }}
    {# stylesheet_link("//cdn.datatables.net/plug-ins/1.10.6/integration/bootstrap/3/dataTables.bootstrap.css", false) #}
    {{ stylesheet_link("assets/datatables/css/dataTables.bootstrap.css") }}

    {{ stylesheet_link("assets/select2/css/select2.min.css") }}
    {{ stylesheet_link("assets/select2/css/select2-bootstrap.css") }}

    {{ stylesheet_link("assets/bootstrap-tagsinput/css/bootstrap-tagsinput.css") }}

    <!-- jQuery -->
    {{ javascript_include("assets/jquery/jquery.min.js") }}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    body {
        background-color: #fff;
        padding-top: 70px;
    }
    .bootstrap-tagsinput {
        width: 100% !important;
    }

    </style>

</head>

<body>

    {{ content() }}

    <!-- Bootstrap Core JavaScript -->
    {{ javascript_include("assets/bootstrap/js/bootstrap.min.js") }}

    {{ javascript_include("assets/select2/js/select2.min.js") }}

    {{ javascript_include("assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js") }}
    {{ javascript_include("assets/bootstrap-datepicker/locales/bootstrap-datepicker.vi.min.js") }}

    {# javascript_include("//cdn.datatables.net/1.10.6/js/jquery.dataTables.min.js", false) }}
    {{ javascript_include("//cdn.datatables.net/plug-ins/1.10.6/integration/bootstrap/3/dataTables.bootstrap.js", false) #}
    {{ javascript_include("assets/datatables/js/jquery.dataTables.min.js") }}
    {{ javascript_include("assets/datatables/js/dataTables.bootstrap.js") }}

    {{ javascript_include("assets/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js") }}

    <script type="text/javascript">
    $(document).ready(function () {
        $(".btndemo").click(function () {
            $(this).parent().siblings().find("ul").collapse('hide');
        });

        $("#goback").click(function () {
            history.back();
        });

        $(".data-table").dataTable({
            "oLanguage": {
                "sSearch": "Lọc:",
                "sEmptyTable": "Không có dữ liệu.",
                "sZeroRecords": "Không có dữ liệu.",
                "sInfo": "Hiển thị _START_ - _END_ bản ghi trong tổng số _TOTAL_.",
                "sLengthMenu": "Hiển thị _MENU_ bản ghi."
            }
        });

        $(".datepicker").datepicker({
            format: "yyyy/mm/dd",
            todayBtn: "linked",
            language: "vi",
            autoclose: true,
            todayHighlight: true,
            orientation: "auto",
        });

        //Add Hover effect to menus
        $('ul.nav li.dropdown').hover(function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeIn();
        }, function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeOut();
        });

        $('.select2').select2();
    });
    </script>

</body>

</html>
