<div class="jumbotron">
    <div class="container">
        <img src="{{ url('assets/img/logo.gif') }}" class="img-responsive" alt="Image">
        <h2>Chào mừng đến với website hội nhà báo Việt Nam</h2>
        <p></p>
        <p></p>
    </div>
</div>

{{ flash.output() }}
{{ content() }}

<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header"><a href="{{url('tacpham/show')}}">Nhà báo</a></h2>
    </div>
</div>
{% for index, member in topMembers %}
{% if index is even %}
<div class="row">
{% endif %}
    <div class="col-md-6 portfolio-item">
        <a href="{{ url("u/" ~ member['username']) }}">
            <div class="journalist-info">
                <img class="img-responsive" src="{{ member['avatar'] }}" alt="Avatar" style="width: 180px; height: 200px;">
                <div class="information">
                    <h5>{{ member['name'] }}</h5>
                    <p>{{ member['biography'] }}</p>
                    <a class="btn btn-primary" href="{{ url("u/" ~ member['username']) }}">Xem thêm</a>
                </div>
            </div>
        </a>
    </div>
{% if index is odd %}
</div>
{% endif %}
{% endfor %}

<!-- /.row -->
<hr>
<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header"><a href="{{url('tacpham/show')}}">Tác phẩm</a></h2>
    </div>
    {% for paper in papers %}
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <div class="panel panel-default" style="height: 200px; overflow: hidden;">
              <div class="panel-heading">
                    <span class="panel-title"><a href="{{url(paper.getLink())}}"><i class="fa fa-external-link"></i> {{ paper.getName() }}</a></span>
              </div>
              <div class="panel-body">
                    <?php echo substr($paper->getInfo(), 0, 100); ?>...
              </div>
        </div>
    </div>
    {% endfor %}
</div>
<!-- /.row -->
<hr>
<!-- Footer -->
<footer>
    <div class="row">
        <div class="col-lg-12">
            <p>Copyright &copy; Hội nhà báo Việt Nam 2015</p>
        </div>
    </div>
    <!-- /.row -->
</footer>
