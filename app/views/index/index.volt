<div class="jumbotron">
    <div class="container">
        <img src="{{ url('assets/img/logo.gif') }}" class="img-responsive" alt="Image">
        <h2>Chào mừng đến với website hội nhà báo Việt Nam</h2>
        <p></p>
        <p></p>
    </div>
</div>

{{ flash.output() }}
{{ content() }}

<div class="row">
    <!-- Blog Entries Column -->
    <div class="col-md-7">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><a href="{{url('hoivien/show')}}"><i class="fa fa-clock-o fa-fw"></i> Thành viên của hội <span class="badge">{{ totalHoivien }}</span></a></h3>
            </div>
            <div class="panel-body">
                {# include "partials/recent_paper.volt" #}
                <!-- <div class="text-right">
                    <a href="#">Xem tất cả <i class="fa fa-arrow-circle-right"></i></a>
                </div> -->
                <div class="col-md-12">
                    <h4>Các nhà báo nổi bật</h4>

                    <!-- First Blog Post -->
                    {% for member in topMembers %}
                    <div class="col-md-4 portfolio-item">
                        <a href="{{ url("u/" ~ member['username']) }}">
                            <img class="img-responsive" src="{{url(member['avatar'])}}" alt="Avatar" style="width: 200px; height: 200px;">
                        </a>
                        <h5>
                            <a href="{{ url("u/" ~ member['username']) }}">
                                {{ member['name'] }}
                            </a>
                        </h5>
                        <p>{{ member['biography'] }} ...</p>
                    </div>
                    {% endfor %}
                    <hr>

                </div>
                <div class="col-md-12">
                    <h4>Các nhà báo mới</h4>

                    <!-- First Blog Post -->
                    {% for member in newMembers %}
                    <div class="col-md-4 portfolio-item">
                        <a href="{{ url("u/" ~ member.username) }}">
                            <img class="img-responsive" src="{{url(member.avatar)}}" alt="Avatar" style="width: 200px; height: 200px;">
                        </a>
                        <h5>
                            <a href="{{ url("u/" ~ member.username) }}">
                                {{ member.name }}
                            </a>
                        </h5>
                        <p><?php echo mb_substr($member->biography, 0, 50)?> ...</p>
                    </div>
                    {% endfor %}
                    <hr>

                </div>
            </div>
        </div>
    </div>

    <!-- Blog Sidebar Widgets Column -->
    <div class="col-md-5">
        <!-- Blog Categories Well -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><a href="{{url('tacpham/show')}}"><i class="fa fa-clock-o fa-fw"></i> Thống kê tác phẩm <span class="badge">{{ totalTacpham }}</span></a></h3>
            </div>
            <div class="panel-body">
                {# include "partials/recent_paper.volt" #}
                <!-- <div class="text-right">
                    <a href="#">Xem tất cả <i class="fa fa-arrow-circle-right"></i></a>
                </div> -->
                <div class="col-md-12">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <h4>Tác phẩm mới</h4>
                        {% set inc = 0 %}
                        {% for paper in papers %}
                            {% set inc = inc + 1 %}
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading{{inc}}">
                                <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{inc}}" aria-expanded="true" aria-controls="collapse{{inc}}">
                                    <i class="fa fa-fw fa-newspaper-o"></i> {{ paper.getName() }}
                                </a>
                                </h4>
                            </div>
                            <div id="collapse{{inc}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{inc}}">
                                <div class="panel-body">
                                    {{ paper.getInfo() }}
                                    <a href="{{url(paper.getLink())}}" title="Xem thêm" target="_blank">Xem thêm <i class="fa fa-external-link"></i></a>
                                </div>
                            </div>
                        </div>
                        {% endfor %}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- </row -->
<hr>

<!-- Footer -->
<footer>
    <div class="row">
        <div class="col-lg-12">
            <p>Copyright &copy; Hội nhà báo Việt Nam 2015</p>
        </div>
    </div>
</footer>
