<div class="jumbotron">
    <div class="container">
        <img src="{{ url('assets/img/logo.gif') }}" class="img-responsive" alt="Image">
        <h2>Chào mừng đến với website hội nhà báo Việt Nam</h2>
        <p></p>
        <p></p>
    </div>
</div>

{{ flash.output() }}
{{ content() }}

<div class="row">
    <div class="col-md-4">
        <div class="panel panel-primary" style="height: 200px;">
            <div class="panel-heading">
                <h4><i class="fa fa-fw fa-history"></i>Lịch sử hội</h4>
            </div>
            <div class="panel-body">
                <p>Ngày 21/4/1950, Hội Những người viết báo Việt Nam đã ra đời tại thôn Roòng Khoa, xã Điềm Mặc thuộc An toàn khu Định Hóa, Thái Nguyên nay là Hội Nhà báo Việt Nam...</p>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-primary" style="height: 200px;">
            <div class="panel-heading">
                <h4><i class="fa fa-fw fa-info"></i> Giới thiệu hội</h4>
            </div>
            <div class="panel-body">
                <p>Hội nhà báo Việt Nam là tổ chức chính trị, xã hội, nghề nghiệp của những người hoạt động trong lĩnh vực báo chí trong phạm vi cả nước và trong các quan hệ trao đổi với các tổ chức báo chí quốc tế... </p>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-primary" style="height: 200px;">
            <div class="panel-heading">
                <h4><i class="fa fa-fw fa-paper-plane"></i> Quy mô hội</h4>
            </div>
            <div class="panel-body">
                <p>Hội nhà báo Việt nam có các tổ chức, thành viên ở hầu hết các tính, thành phố trong cả nước. Đến nay hội đã có {{ totalHoivien }} hội viên, với hơn {{ totalTacpham }} tác phẩm báo chí khác nhau.</p>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">Các gương mặt nhà báo tiêu biểu</h2>
    </div>
    {% for member in topMembers %}
    <div class="col-md-3 col-sm-6">
        <a href="{{ url("u/" ~ member['username']) }}">
            <img class="img-responsive img-portfolio img-hover" src="{{ url(member['avatar'])}}" alt="Avatar" style="width: 200px; height: 200px;">
        </a>
    </div>
    {% endfor %}
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">Tác phẩm mới</h2>
    </div>
    {% for paper in papers %}
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <div class="panel panel-default" style="height: 200px;">
              <div class="panel-heading">
                    <span class="panel-title" style="color: blue !important;"><a href="{{url(paper.getLink())}}"><i class="fa fa-external-link"></i> {{ paper.getName() }}</a></span>
              </div>
              <div class="panel-body">
                    <?php echo substr($paper->getInfo(), 0, 100); ?>...
              </div>
        </div>
    </div>
    {% endfor %}
</div>
<!-- /.row -->

<hr>
<!-- Footer -->
<footer>
    <div class="row">
        <div class="col-lg-12">
            <p>Copyright &copy; Hội nhà báo Việt Nam 2015</p>
        </div>
    </div>
</footer>
