<li class='dropdown'>
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-fw fa-users"></i> Giao diện <span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
        <li>
            <a href={{url("index/settheme/1")}}><i class='fa fa-fw fa-{% if this.session.theme == 1 %}check{% endif %}'></i> Theme 1</a>
        </li>
        <li>
            <a href={{url("index/settheme/2")}}><i class='fa fa-fw fa-{% if this.session.theme == 2 %}check{% endif %}'></i> Theme 2</a>
        </li>
        <li>
            <a href={{url("index/settheme/3")}}><i class='fa fa-fw fa-{% if this.session.theme == 3 %}check{% endif %}'></i> Theme 3</a>
        </li>
    </ul>
</li>
