<!-- views/partials/recent_paper.volt -->
<div class="list-group">
{% for paper in papers %}
    <a href="#" class="list-group-item">
        <span class="badge">{{ paper.getAddedAt() }}</span>
        <i class="fa fa-fw fa-calendar"></i> {{ paper.getName() }}
    </a>
{% endfor %}
</div>
