<!-- views/partials/recent_member.volt -->
<table class="table table-bordered table-hover table-striped">
    <thead>
        <tr>
            <th>Tên</th>
            <th>Email</th>
            <th>Ngày tham gia</th>
        </tr>
    </thead>
    <tbody>
    {% for member in members %}
        <tr>
            <td><a href="{{ url("u/" ~ member.getUsername()) }}">{{ member.getName() }}</a></td>
            <td>{{ member.getEmail() }}</td>
            <td><span class="label label-default">{{ member.getCreatedOn() }}</span></td>
        <tr>
    {% endfor %}
    </tbody>
</table>
