<!-- views/partials/login_modal.volt -->
<div class="modal fade" id="login-form">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Đăng nhập</h4>
            </div>
            <div class="modal-body">
                <form action="{{url('profile/login')}}" method="post" accept-charset="utf-8">
                    <div class="form-group">
                        {{ login_form.get('username') }}
                    </div>

                    <div class="form-group">
                        {{ login_form.get('password') }}
                    </div>

                    <div class="form-group">
                        {{ login_form.get('remember') }}
                        <label for="remember"> {{ login_form.get('remember').getLabel() }}</label>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary btn-block" value="Đăng nhập">
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    $('.modal').on('shown.bs.modal', function() {
        $(this).find('[autofocus]').focus();
    });
</script>
