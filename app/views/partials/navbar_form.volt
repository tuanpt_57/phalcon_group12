<!-- views/partials/navbar_form.volt -->
{# set ctrlName = router.getControllerName() #}
<!-- <form class="navbar-form navbar-nav" role="search" action="/search/{# !ctrlName ? "index" : ctrlName #}/search" method="post"> -->
<form class="navbar-form navbar-nav" role="search" action="{{url('search')}}" method="post">
    <div class="form-group">
        <input type="text" name="search" class="form-control" placeholder="Tìm kiếm" size="12" required>
    </div>
</form>
<script>
    var form = document.forms[0];
    form.addEventListener("submit", function (e) {
        e.preventDefault();
        window.location.href = "{{ url('search/') }}" + this.search.value;
        return false;
    });
</script>
