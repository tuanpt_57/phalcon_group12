<!-- views/partials/page_heading.volt -->
<!-- Page Heading -->
{% if pageHeader is defined %}
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            {{ pageHeader }}
        </h1>
        {% if breadCrumb is defined %}
        <ol class="breadcrumb">
            <?php
            $breadCrumb = explode("/", $breadCrumb);
            ?>
            <li class="active">
                <i class="fa fa-dashboard"></i>
            </li>
            {% for br in breadCrumb %}
            {% if !loop.last %}
                <li>{{ link_to(dispatcher.getControllerName(), br) }}</li>
            {% endif %}
            {% if loop.last %}
                <li class="active">{{ br }}</li>
            {% endif %}
            {% endfor %}
        </ol>
        {% endif %}
    </div>
</div> <!-- /.row -->
{% endif %}
