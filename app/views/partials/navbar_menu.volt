<!-- views/partials/navbar_menu.volt -->
{% set uri = this.request.getURI() %}
{#// Biến $identity được truyền từ ControllerBase, xác định định danh hiện tại#}
{% set roleId = identity['profile'] %}

{% if roleId == 1 %}
<li {{ ("dashboard" in uri) ? "class='active'" : "" }}>
    {{ link_to("dashboard", "<i class='fa fa-fw fa-dashboard'></i> Dashboard", "class" : "sidebarlink") }}
</li>
{% endif %}

<li {{ ("hoivien" in uri) or ("role" in uri) ? "class='dropdown active'" : "class='dropdown'" }}>
    {% if roleId != 3 %}
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-fw fa-users"></i> Hội viên <span class="caret"></span></a>
    <ul id="hoivien" class="dropdown-menu" role="menu">
        <li>
            {{ link_to("hoivien", "<i class='fa fa-fw fa-list'></i> Danh sách", "class" : "sidebarlink") }}
        </li>
        {% if roleId == 1 %}
        <li>
            {{ link_to("hoivien/listadmin", "<i class='fa fa-fw fa-key'></i> Admin", "class" : "sidebarlink") }}
        </li>
        {% endif %}
        <li>
            {{ link_to("role", "<i class='fa fa-fw fa-lock'></i> Vai trò", "class" : "sidebarlink") }}
        </li>
        <li>
            {{ link_to("hoivien/show", "<i class='fa fa-fw fa-home'></i> Trang hội viên", "class" : "sidebarlink") }}
        </li>
    </ul>
    {% else %}
    {{ link_to("hoivien/show", "<i class='fa fa-fw fa-users'></i> Nhà báo", "class" : "sidebarlink") }}
    {% endif %}
</li>

<li {{ ("tacpham" in uri) or ("chude" in uri) ? "class='dropdown active'" : "class='dropdown'" }}>
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-fw fa-newspaper-o"></i> Tác phẩm <span class="caret"></span></a>
    <ul id="tacpham" class="dropdown-menu" role="menu">
        <li>
            {{ link_to("tacpham", "<i class='fa fa-fw fa-list'></i> Danh sách", "class" : "sidebarlink") }}
        </li>
        <li>
            {% if roleId != 3 %}
                {{ link_to("chude", "<i class='fa fa-fw fa-tags'></i> Chủ đề", "class" : "sidebarlink") }}
            {% endif %}
        </li>
        <li>
            {{ link_to("tacpham/show", "<i class='fa fa-fw fa-home'></i> Trang tác phẩm", "class" : "sidebarlink") }}
        </li>
    </ul>
</li>
<li {{ ("giaithuong" in uri) or ("loaigiaithuong" in uri) ? "class='dropdown active'" : "class='dropdown'" }}>
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-fw fa-trophy"></i> Giải thưởng <span class="caret"></span></a>
    <ul id="giaithuong" class="dropdown-menu" role="menu">
        <li>
            {{ link_to("giaithuong", "<i class='fa fa-fw fa-list'></i> Danh sách", "class" : "sidebarlink") }}
        </li>
        <li>
            {% if roleId != 3 %}
                {{ link_to("loaigiaithuong", "<i class='fa fa-fw fa-tags'></i> Loại giải thưởng", "class" : "sidebarlink") }}
            {% endif %}
        </li>
    </ul>
</li>
