<!-- views/partials/user_menu.volt -->
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ userName }} <b class="caret"></b></a>
    <ul class="dropdown-menu">
        <li>
            <a href="{{ url('u/')}}"><i class="fa fa-fw fa-user"></i> Profile</a>
        </li>
        <li class="divider"></li>
        <li>
            <a href={{url("index/settheme/1")}}><i class='fa fa-fw fa-{% if this.session.theme == 1 %}check{% endif %}'></i> Theme 1</a>
        </li>
        <li>
            <a href={{url("index/settheme/2")}}><i class='fa fa-fw fa-{% if this.session.theme == 2 %}check{% endif %}'></i> Theme 2</a>
        </li>
        <li>
            <a href={{url("index/settheme/3")}}><i class='fa fa-fw fa-{% if this.session.theme == 3 %}check{% endif %}'></i> Theme 3</a>
        </li>
        <li class="divider"></li>
        <li>
            <a href="{{url('profile/logout')}}"><i class="fa fa-fw fa-power-off"></i> Đăng xuất</a>
        </li>
    </ul>
</li>
