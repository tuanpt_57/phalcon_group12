<!-- views/partials/home_menu.volt -->
{% set uri = this.request.getURI() %}

<li {{ ("hoivien" in uri) ? "class='active'" : "" }}>
    {{ link_to("hoivien/show", "<i class='fa fa-fw fa-users'></i> Nhà báo", "class" : "sidebarlink") }}
</li>

<li {{ ("tacpham" in uri) ? "class='active'" : "" }}>
    {{ link_to("tacpham/show", "<i class='fa fa-fw fa-newspaper-o'></i> Tác phẩm", "class" : "sidebarlink") }}
</li>
