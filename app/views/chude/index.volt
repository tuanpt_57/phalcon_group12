<div class="row">
    <div class="col-lg-12">
        {{ flash.output() }}
        {{ content() }}
        <p>
            {{ link_to("tacpham", "<i class='fa fa-fw fa-arrow-circle-left'></i> Trở về", "class" : "btn btn-default") }}
        </p>

        <form id="deleteSelected" action="{{ url('chude/delete') }}" method="post" accept-charset="utf-8">
            <p>
                Tổng số chủ đề: <span class="badge">{{ subjects | length }}</span> -
                {{ link_to("chude/add", "<i class='fa fa-plus'></i> Thêm mới", "class" : "btn btn-sm btn-primary") }}
                <button type="submit" class="btn btn-sm btn-danger" name="submit">
                    <span class="fa fa-trash-o"></span> Xóa các mục đã chọn
                </button>
            </p>
            {% if subjects | length %}
            <table id="subjects" class="table table-bordered table-hover data-table">
                <thead>
                    <tr>
                        <th><input type="checkbox" id="chkSelectAll"></th>
                        <th>ID</th>
                        <th>Tên</th>
                        <th>Số tác phẩm</th>
                        <th style="min-width: 50px;"></th>
                    </tr>
                </thead>
                <tbody>
                    {% for subject in subjects %}
                    <tr>
                        <td><input type="checkbox" name="chkSelect[]" value="{{ subject.getId() }}"></td>
                        <td>{{ subject.getId() | e }}</td>
                        <td>{{ subject.getName() | e }}</td>
                        <td>{{ subject.countTacpham(subject.getId()) }}</td>
                        <td>
                            {{ link_to("chude/edit/" ~ subject.getId(), "<span class='fa fa-pencil'></span>", "class" : "btn btn-sm btn-default") }}
                            {{ link_to("chude/delete/" ~ subject.getId(), "<span class='fa fa-trash-o'></span>", "class" : "btn btn-sm btn-danger") }}
                        </td>
                    </tr>
                    {% endfor %}
                </tbody>
            </table>
            {% endif %}
        </form>

        <script>
            var chkSelectAll = document.getElementById('chkSelectAll');
            var chkSelects = document.getElementsByName('chkSelect[]');
            chkSelectAll.addEventListener('click', function () {
                for (var i = 0; i < chkSelects.length; i++) {
                    chkSelects[i].checked = this.checked;
                };
            });
            var deleteSelected = document.getElementById('deleteSelected');
            deleteSelected.addEventListener("submit", function (e) {
                if (!confirm("Bạn có chắc chắn?")) {
                    e.preventDefault();
                    return false;
                }
            });
        </script>
    </div>
</div>
