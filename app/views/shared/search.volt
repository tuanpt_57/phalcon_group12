{{ content() }}

<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-primary">
              <div class="panel-heading">
                    <h3 class="panel-title">Hội viên <span class="badge">{{ members | length }}</span></h3>
              </div>
              <div class="panel-body">
                    <div class="list-group">
                    {% for member in members %}
                        <a href="{{ url("u/" ~ member.getUsername()) }}" class="list-group-item">
                            <span class="badge">{{ member.getUsername() }}</span>
                            <i class="fa fa-fw fa-calendar"></i> {{ member.getName() }}
                        </a>
                    {% endfor %}
                    </div>
                    <div class="text-right">
                        <a href="{{url('hoivien/show')}}">Xem tất cả <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
              </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-primary">
              <div class="panel-heading">
                    <h3 class="panel-title">Tác phẩm <span class="badge">{{ papers | length }}</span></h3>
              </div>
              <div class="panel-body">
                    <div class="list-group">
                    {% for paper in papers %}
                        <a href="#" class="list-group-item">
                            <span class="badge">{{ paper.getDateComposed() }}</span>
                            <i class="fa fa-fw fa-calendar"></i> {{ paper.getName() }}
                        </a>
                    {% endfor %}
                    </div>
                    <div class="text-right">
                        <a href="{{url('tacpham/show')}}">Xem tất cả <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
              </div>
        </div>
    </div>
</div>
