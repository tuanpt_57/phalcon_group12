<div class="row">
    <div class="col-lg-12">
        {{ flash.output() }}
        {{ content() }}
        <p>
            {{ link_to("loaigiaithuong", "<i class='fa fa-fw fa-arrow-circle-left'></i> Trở về", "class" : "btn btn-default") }}
        </p>
        <form id="deleteSelected" action="{{url('loaigiaithuong/delete')}}" method="post" accept-charset="utf-8">
            <p>
                Các loại giải thưởng: <span class="badge">{{ types | length }}</span> -
                {{ link_to("loaigiaithuong/add", "<i class='fa fa-plus'></i> Thêm mới", "class" : "btn btn-sm btn-primary") }}
                <button type="submit" class="btn btn-sm btn-danger" name="submit">
                    <span class="fa fa-trash-o"></span> Xóa các mục đã chọn
                </button>
            </p>
            {% if types | length %}
            <table id="types" class="table table-bordered table-hover data-table">
                <thead>
                    <tr>
                        <th><input type="checkbox" id="chkSelectAll"></th>
                        <th>ID</th>
                        <th style="min-width: 160px;">Tên</th>
                        <th>Thông tin</th>
                        <th style="min-width: 60px;">Số giải</th>
                        <th style="min-width: 50px;"></th>
                    </tr>
                </thead>
                <tbody>
                    {% for type in types %}
                    <tr>
                        <td><input type="checkbox" name="chkSelect[]" value="{{ type.getId() }}"></td>
                        <td>{{ type.getId() | e }}</td>
                        <td>{{ type.getName() | e }}</td>
                        <td>{{ type.getInfo() | nl2br }}</td>
                        <td>{{ type.countGiaithuong(type.getId()) }}</td>
                        <td>
                            {{ link_to("loaigiaithuong/edit/" ~ type.getId(), "<span class='fa fa-pencil'></span>", "class" : "btn btn-sm btn-default") }}
                            {{ link_to("loaigiaithuong/delete/" ~ type.getId(), "<span class='fa fa-trash-o'></span>", "class" : "btn btn-sm btn-danger") }}
                        </td>
                    </tr>
                    {% endfor %}
                </tbody>
            </table>
            {% endif %}
        </form>

        <script>
            var chkSelectAll = document.getElementById('chkSelectAll');
            var chkSelects = document.getElementsByName('chkSelect[]');
            chkSelectAll.addEventListener('click', function () {
                for (var i = 0; i < chkSelects.length; i++) {
                    chkSelects[i].checked = this.checked;
                };
            });
            var deleteSelected = document.getElementById('deleteSelected');
            deleteSelected.addEventListener("submit", function (e) {
                if (!confirm("Bạn có chắc chắn?")) {
                    e.preventDefault();
                    return false;
                }
            });
        </script>
    </div>
</div>
