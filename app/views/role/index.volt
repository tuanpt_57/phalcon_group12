<div class="row">
    <div class="col-lg-12">
        {{ flash.output() }}
        {{ content() }}
        <p>
            {{ link_to("hoivien", "<i class='fa fa-fw fa-arrow-circle-left'></i> Trở về", "class" : "btn btn-default") }}
        </p>
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th style="min-width: 20px;">ID</th>
                    <th>Tên</th>
                    <th>Mô tả</th>
                    <th style="min-width: 160px;">Số lượng hội viên</th>
                    {% if roleId == 1 %}
                    <th style="min-width: 50px;"></th>
                    {% endif %}
                </tr>
            </thead>
            <tbody>
                {% for role in roles %}
                <tr>
                    <td>{{ role.getId() | e }}</td>
                    <td>{{ role.getName() | e }}</td>
                    <td>{{ role.getInfo() | e }}</td>
                    <td>{{ role.countHoivien(role.getId()) }}</td>
                    {% if roleId == 1 %}
                    <td>
                        {{ link_to("role/edit/" ~ role.id, "<span class='fa fa-pencil'></span>", "class" : "btn btn-sm btn-default") }}
                    </td>
                    {% endif %}
                </tr>
                {% endfor %}
            </tbody>
        </table>
    </div>
</div>
