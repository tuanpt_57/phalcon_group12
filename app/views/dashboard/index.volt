{{ flash.output() }}
{{ content() }}
<div class="row">
    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-users fa-3x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="badge">{{ totalHoivien }}</div>
                                <div>Hội viên</div>
                            </div>
                        </div>
                    </div>
                    {{ link_to("hoivien",
                        "<div class='panel-footer'>
                            <span class='pull-left'>Chi tiết</span>
                            <span class='pull-right'><i class='fa fa-arrow-circle-right'></i></span>
                            <div class='clearfix'></div>
                        </div>")
                    }}
                </div>
            </div>
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-newspaper-o fa-3x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="badge">{{ totalTacpham }}</div>
                                <div>Tác phẩm</div>
                            </div>
                        </div>
                    </div>
                    {{ link_to("tacpham",
                        "<div class='panel-footer'>
                            <span class='pull-left'>Chi tiết</span>
                            <span class='pull-right'><i class='fa fa-arrow-circle-right'></i></span>
                            <div class='clearfix'></div>
                        </div>")
                    }}
                </div>
            </div>
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-tags fa-3x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="badge">{{ totalChude }}</div>
                                <div>Chủ đề báo chí</div>
                            </div>
                        </div>
                    </div>
                    {{ link_to("chude",
                        "<div class='panel-footer'>
                            <span class='pull-left'>Chi tiết</span>
                            <span class='pull-right'><i class='fa fa-arrow-circle-right'></i></span>
                            <div class='clearfix'></div>
                        </div>")
                    }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-9 col-sm-9 col-md-9 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Hoạt động hội viên</h3>
            </div>
            <div class="panel-body" style="min-height: 345px;">
                <div class="list-group">
                {% for log in logs %}
                    <span class="list-group-item">
                        <span class="badge">{{ log.getTime() }}</span>
                        <i class="fa fa-fw fa-calendar"></i> {{ log.getInfo() }}
                    </span>
                {% endfor %}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Hội viên mới</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    {% include "partials/recent_member.volt" %}
                </div>
                <div class="text-right">
                    <a href="{{ url('hoivien') }}">Xem tất cả <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Tác phẩm mới</h3>
            </div>
            <div class="panel-body">
                {% include "partials/recent_paper.volt" %}
                <div class="text-right">
                    <a href="{{ url('tacpham') }}">Xem tất cả <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
