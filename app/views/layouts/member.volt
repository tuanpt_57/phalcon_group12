<!-- views/layouts/member.volt -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        {% include "partials/navbar_header.volt" %}

        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-left">
                {% include "partials/navbar_menu.volt" %}
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <!-- Top Menu Items -->
                {% include "partials/navbar_form.volt" %}
                {% include "partials/user_menu.volt" %}
            </ul>
        </div> <!-- /.navbar-collapse -->
    </div>
</nav>

<!-- <div id="page-wrapper"> -->

<div class="container">

    {% include "partials/page_heading.volt" %}
    {{ content() }}

</div> <!-- /.container -->
