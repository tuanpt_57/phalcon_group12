<!-- views/layouts/mod.volt -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        {% include "partials/navbar_header.volt" %}

        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-left">
                {% include "partials/navbar_menu.volt" %}
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <!-- Top Menu Items -->
                {% include "partials/navbar_form.volt" %}
                    <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                        <ul class="dropdown-menu alert-dropdown">
                            <li>
                                <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">View All</a>
                            </li>
                        </ul>
                    </li> -->
                {% include "partials/user_menu.volt" %}
            </ul>
        </div> <!-- /.navbar-collapse -->
    </div>
</nav>

<!-- <div id="page-wrapper"> -->

<div class="container">

    {% include "partials/page_heading.volt" %}
    {{ content() }}

</div> <!-- /.container -->
