<!-- views/layouts/guest.volt -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        {% include "partials/navbar_header.volt" %}

        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-left">
                {% include "partials/home_menu.volt" %}
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <!-- Top Menu Items -->
                {% include "partials/navbar_form.volt" %}

                <li><a data-toggle="modal" href='#login-form'><i class="fa fa-fw fa-sign-in"></i> Đăng nhập</a></li>

                {% include "partials/theme_menu.volt" %}
            </ul>
        </div> <!-- /.navbar-collapse -->
    </div>
</nav>

<!-- <div id="page-wrapper"> -->

<div class="container">

    {% include "partials/page_heading.volt" %}
    {{ content() }}

</div> <!-- /.container -->

{% include "partials/login_modal.volt" %}
