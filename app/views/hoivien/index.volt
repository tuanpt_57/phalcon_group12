<div class="row">
    <div class="col-lg-12">
        {{ flash.output() }}
        {{ content() }}
        <form id="deleteSelected" action="{{url('hoivien/delete')}}" method="post" accept-charset="utf-8">
            <p>
                Tổng số hội viên: <span class="badge">{{ members | length }}</span>
                {% if roleId == 1 %}
                - {{ link_to("hoivien/add", "<i class='fa fa-plus'></i> Thêm mới", "class" : "btn btn-sm btn-primary") }}
                <button type="submit" class="btn btn-sm btn-danger" name="submit">
                    <span class="fa fa-trash-o"></span> Xóa các mục đã chọn
                </button>
                {% endif %}
            </p>
            {% if members | length %}
            <table id="members" class="table table-bordered table-hover data-table">
                <thead>
                    <tr>
                        {% if roleId == 1 %}
                        <th style="min-width: 20px;"><input type="checkbox" id="chkSelectAll"></th>
                        {% endif %}
                        <th>ID</th>
                        <th>User</th>
                        <th>Tên</th>
                        <th>Ngày sinh</th>
                        <th>Quê quán</th>
                        <th>Email</th>
                        <th>Trạng thái</th>
                        <th>Số tác phẩm</th>
                        <th>Số giải thưởng</th>
                        {% if roleId == 1 %}
                        <th style="min-width: 90px;"></th>
                        {% endif %}
                    </tr>
                </thead>
                <tbody>
                    {% for member in members %}
                    <tr>
                        {% if roleId == 1 %}
                        <td>
                            {% if member.getId() != '1' %}
                                <input type="checkbox" name="chkSelect[]" value="{{ member.getId() }}">
                            {% else %}
                                <input type="checkbox" name="noselect" value="{{ member.getId() }}" disabled="disabled">
                            {% endif %}
                        </td>
                        {% endif %}
                        <td>{{ member.getId() | e }}</td>
                        <td><a href="{{ url("u/" ~ member.getUsername()) }}">{{ member.getUsername() }}</a></td>
                        <td>{{ member.getName() | e }}</td>
                        <td>{{ member.getBirthday() | e }}</td>
                        <td>{{ member.getCountry() | e }}</td>
                        <td>{{ member.getEmail() | e }}</td>
                        <td>
                            {% if member.getIsActive() == '1' %}
                                {{ link_to("hoivien/setinactive/" ~ member.getId(), "<span class='label label-success'><i class='fa fa-check'></i> Active</span>", "class" : "btn btn-xs", "title": "Hủy kích hoạt") }}
                            {% else %}
                                {{ link_to("hoivien/setactive/" ~ member.getId(), "<span class='label label-warning'><i class='fa fa-times'></i> Inactive</span>", "class" : "btn btn-xs", "title": "Kích hoạt") }}
                            {% endif %}
                        </td>
                        <td>{{ member.countTacpham(member.getId()) }}</td>
                        <td>{{ member.countGiaithuong(member.getId()) }}</td>
                        {% if roleId == 1 %}
                        <td>
                            {{ link_to("hoivien/edit/" ~ member.getId(), "<span class='fa fa-pencil'></span>", "class" : "btn btn-sm btn-default") }}
                            {% if member.getId() != '1' %}
                                {{ link_to("hoivien/setmod/" ~ member.getId(), "<span class='fa fa-key'></span>", "class" : "btn btn-sm btn-info") }}
                                {{ link_to("hoivien/delete/" ~ member.getId(), "<span class='fa fa-trash-o'></span>", "class" : "btn btn-sm btn-danger") }}
                            {% endif %}
                        </td>
                        {% endif %}
                    </tr>
                    {% endfor %}
                </tbody>
            </table>
            {% endif %}
        </form>

        <script>
            var chkSelectAll = document.getElementById('chkSelectAll');
            var chkSelects = document.getElementsByName('chkSelect[]');
            chkSelectAll.addEventListener('click', function () {
                for (var i = 0; i < chkSelects.length; i++) {
                    chkSelects[i].checked = this.checked;
                };
            });
            var deleteSelected = document.getElementById('deleteSelected');
            deleteSelected.addEventListener("submit", function (e) {
                if (!confirm("Bạn có chắc chắn?")) {
                    e.preventDefault();
                    return false;
                }
            });
            $('.btn-danger').on('click', function (e) {
                if (!confirm("Bạn có chắc chắn?")) {
                    e.preventDefault();
                    return false;
                }
            });
        </script>
    </div>
</div>
