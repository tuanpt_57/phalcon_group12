{{ flash.output() }}
{{ content() }}

<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">Nhà báo</h2>
    </div>
    {% for member in members.items %}
    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
        <div class="panel panel-default" style="height: 400px; overflow: hidden;">
            <div class="panel-heading">
                <span class="panel-title"><i class="fa fa-user"></i> <a href="{{ url("u/" ~ member.getUsername()) }}">{{ member.getName() }}</a></span>
            </div>
            <div class="panel-body text-center">
                <a href="{{ url("u/" ~ member.getUsername()) }}"><img class="img-responsive" src="{{url(member.getAvatar())}}" alt="Avatar" style="width: 250px; height: 260px;"></a>
                <h5>
                    <a href="{{ url("u/" ~ member.getUsername()) }}">
                        {{ member.getName() }}
                    </a>
                </h5>
                <?php echo substr($member->getBiography(), 0, 100); ?>...
            </div>
        </div>
    </div>
    {% endfor %}
</div>
<div class="row">
    <div class="col-xs-12 text-center">
        {{ paginator.renderBootstrap() }}
    </div>
</div>
