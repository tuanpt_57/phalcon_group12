<div class="row">
    <div class="col-lg-12">
        {{ flash.output() }}
        {{ content() }}
        <p>
            Danh sách quản trị viên: <span class="badge">{{ members | length }}</span> -
            {{ link_to("hoivien", "<i class='fa fa-fw fa-arrow-circle-left'></i> Trở về", "class" : "btn btn-default") }}
        </p>
        {% if members | length %}
            <table id="members" class="table table-bordered table-hover data-table">
                <thead>
                    <tr>
                        <th style="min-width: 20px;">ID</th>
                        <th>User</th>
                        <th>Tên</th>
                        <th>Ngày sinh</th>
                        <th>Email</th>
                        <th>Trạng thái</th>
                        <th style="min-width: 50px;"></th>
                    </tr>
                </thead>
                <tbody>
                    {% for member in members %}
                    <tr>
                        <td>{{ member.getId() | e }}</td>
                        <td>{{ member.getUsername() | e }}</td>
                        <td>{{ member.getName() | e }}</td>
                        <td>{{ member.getBirthday() | e }}</td>
                        <td>{{ member.getEmail() | e }}</td>
                        <td>
                            {% if member.getIsActive() == '1' %}
                                <span class="label label-success">Active</span>
                            {% else %}
                                <span class="label label-warning">Inactive</span>
                            {% endif %}
                        </td>
                        <td>
                            {{ link_to("hoivien/edit/" ~ member.getId(), "<span class='fa fa-pencil'></span>", "class" : "btn btn-sm btn-default") }}
                            {% if member.getId() != '1' %}
                                {{ link_to("hoivien/unsetmod/" ~ member.getId(), "<span class='fa fa-trash-o'></span>", "class" : "btn btn-sm btn-danger") }}
                            {% endif %}
                        </td>
                    </tr>
                    {% endfor %}
                </tbody>
            </table>
        {% endif %}
    </div>
</div>
