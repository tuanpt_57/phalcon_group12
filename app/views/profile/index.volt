<div class="row">
    <div class="col-lg-12 text-center">
        {{ flash.output() }}
        {{ content() }}
    </div>
</div>
{% if profile %}
    {% if identity is defined %}
        {% if identity['id'] == profile.getId() %}
            {% set edit = true %}
        {% endif %}
    {% endif %}
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="page-header">
                <h4>
                {% if profile.getGender() == 'male' %}
                    <i class="fa fa-male"></i> {{ profile.getName() }}
                {% else %}
                    <i class="fa fa-female"></i> {{ profile.getName() }}
                {% endif %}

                {% if edit is defined and edit %}
                    {{ link_to("profile/edit/", "<span class='fa fa-pencil'></span>", "class" : "btn btn-xs btn-success") }}
                {% endif %}
                </h4>
            </div>
            <table>
                <tr>
                    <td style="padding-right: 15px;" rowspan="4"><img src="{{ url(profile.getAvatar()) }}" class="img-responsive" alt="Avatar" style="width: 180px; height: 200px"></td>
                    <td><label>Quê quán:</label> {{ profile.getCountry() }}</td>
                </tr>
                <tr>
                    <td><label>Ngày sinh:</label> {{ profile.getBirthday() }}</td>
                </tr>
                <tr>
                    <td><label>Email:</label> {{ profile.getEmail() }}</td>
                </tr>
                <tr>
                {% if edit is defined and edit %}
                    <td><label>Số CMND:</label> {{ profile.getIdCard() }}</td>
                {% else %}
                    <td>&nbsp;</td>
                {% endif %}
                </tr>
            </table>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="page-header">
                <h4>Tiểu sử</h4>
            </div>
            <p>{{ profile.getBiography() }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="page-header">
                <h4>
                    Các tác phẩm
                    {% if edit is defined and edit %}
                        {{ link_to("tacpham", "<span class='fa fa-pencil'></span>", "class" : "btn btn-xs btn-success") }}
                    {% endif %}
                </h4>
            </div>
            <div>
                <ul class="list-group">
                    {% for paper in papers %}
                        <li class="list-group-item">
                            <span class="badge">{{ paper.getDateComposed() }}</span>
                            {{ paper.getName() }}
                        </li>
                    {% endfor %}
                </ul>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="page-header">
                <h4>
                    Giải thưởng
                    {% if edit is defined and edit %}
                        {{ link_to("giaithuong", "<span class='fa fa-pencil'></span>", "class" : "btn btn-xs btn-success") }}
                    {% endif %}
                </h4>
            </div>
            <div>
                <ul class="list-group">
                    {% for prize in prizes %}
                        <li class="list-group-item">
                            <span class="badge">{{ prize.getDateRewarded() }}</span>
                            {{ prize.loaigiaithuong.getName() }}
                        </li>
                    {% endfor %}
                </ul>
            </div>
        </div>
    </div>
{% endif %}
