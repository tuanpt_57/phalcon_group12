<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        {{ flash.output() }}
        {{ content() }}

        <form action="{{url('profile/login')}}" method="post" accept-charset="utf-8">
            <div class="form-group">
                {{ login_form.get('username') }}
            </div>

            <div class="form-group">
                {{ login_form.get('password') }}
            </div>

            <div class="form-group">
                {{ login_form.get('remember')}}
                <label for="remember">Remember me</label>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary btn-block" value="Đăng nhập">
            </div>
        </form>
    </div>
</div>
