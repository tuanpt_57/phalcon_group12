<div class="row">
    <div class="col-lg-12">
        {{ flash.output() }}
        {{ content() }}

        <form class="form-horizontal" action="{{ url('giaithuong/create') }}" method="post" accept-charset="utf-8">
            <div class="form-group">
                <div class="col-sm-10 col-md-9 col-lg-10 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
                {{ link_to("giaithuong", "<i class='fa fa-fw fa-arrow-circle-left'></i> Hủy bỏ", "class" : "btn btn-default") }}
                </div>
            </div>
            {{ form.renderAll() }}
            <div class="form-group">
                <div class="col-sm-10 col-md-9 col-lg-10 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
                    <button type="submit" class="btn btn-primary">Lưu</button>
                    <input type="reset" value="Xóa" class="btn btn-warning">
                </div>
            </div>
        </form>
    </div>
</div>
