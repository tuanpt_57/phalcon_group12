<div class="row">
    <div class="col-lg-12">
        {{ flash.output() }}
        {{ content() }}
        <form id="deleteSelected" action="{{ url('giaithuong/delete') }}" method="post" accept-charset="utf-8">
            <p>
                Tổng số giải thưởng: <span class="badge">{{ prizes | length }}</span> -
                {{ link_to("giaithuong/add", "<i class='fa fa-plus'></i> Thêm mới", "class" : "btn btn-sm btn-primary") }}
                <button type="submit" class="btn btn-sm btn-danger" name="submit">
                    <span class="fa fa-trash-o"></span> Xóa các mục đã chọn
                </button>
            </p>
            {% if prizes | length %}
            <table id="prizes" class="table table-bordered table-hover data-table">
                <thead>
                    <tr>
                        <th style="min-width: 20px;"><input type="checkbox" id="chkSelectAll"></th>
                        <th>ID</th>
                        <th>Giải thưởng</th>
                        {% if roleId is defined and roleId != 3 %}
                        <th>Hội viên</th>
                        {% endif %}
                        <th>Ngày trao thưởng</th>
                        <th style="min-width: 50px;"></th>
                    </tr>
                </thead>
                <tbody>
                    {% for prize in prizes %}
                    <tr>
                        <td><input type="checkbox" name="chkSelect[]" value="{{ prize.getId() }}"></td>
                        <td>{{ prize.getId() | e }}</td>
                        <td>{{ prize.loaigiaithuong.getName() | e }}</td>
                        {% if roleId is defined and roleId != 3 %}
                        <td>{{ prize.hoivien.getName() | e}}</td>
                        {% endif %}
                        <td>{{ prize.getDateRewarded() | e }}</td>
                        <td>
                            {{ link_to("giaithuong/edit/" ~ prize.getId(), "<span class='fa fa-pencil'></span>", "class" : "btn btn-sm btn-default") }}
                            {{ link_to("giaithuong/delete/" ~ prize.getId(), "<span class='fa fa-trash-o'></span>", "class" : "btn btn-sm btn-danger") }}
                        </td>
                    </tr>
                    {% endfor %}
                </tbody>
            </table>
            {% endif %}
        </form>

        <script>
            var chkSelectAll = document.getElementById('chkSelectAll');
            var chkSelects = document.getElementsByName('chkSelect[]');
            chkSelectAll.addEventListener('click', function () {
                for (var i = 0; i < chkSelects.length; i++) {
                    chkSelects[i].checked = this.checked;
                };
            });
            var deleteSelected = document.getElementById('deleteSelected');
            deleteSelected.addEventListener("submit", function (e) {
                if (!confirm("Bạn có chắc chắn?")) {
                    e.preventDefault();
                    return false;
                }
            });
            $('.btn-danger').on('click', function (e) {
                if (!confirm("Bạn có chắc chắn?")) {
                    e.preventDefault();
                    return false;
                }
            });
        </script>
    </div>
</div>
