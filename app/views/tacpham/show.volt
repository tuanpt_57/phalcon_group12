<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">Tác phẩm báo chí</h2>
    </div>
    {% for paper in papers.items %}
    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
        <div class="panel panel-default" style="height: 200px; overflow: hidden;">
              <div class="panel-heading">
                    <span class="panel-title"><a href="{{url(paper.getLink())}}" target="_blank"><i class="fa fa-external-link"></i> {{ paper.getName() }}</a></span>
              </div>
              <div class="panel-body">
                    <?php echo substr($paper->getInfo(), 0, 100); ?>...
              </div>
        </div>
    </div>
    {% endfor %}
</div>
<div class="row">
    <div class="col-xs-12 text-center">
        {{ paginator.renderBootstrap() }}
    </div>
</div>
