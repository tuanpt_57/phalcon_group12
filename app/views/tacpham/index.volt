<div class="row">
    <div class="col-lg-12">
        {{ flash.output() }}
        {{ content() }}

        <form id="deleteSelected" action="{{url('tacpham/delete')}}" method="post" accept-charset="utf-8">
            <p>
                Tổng số tác phẩm: <span class="badge">{{ papers | length }}</span> -
                {{ link_to("tacpham/add", "<i class='fa fa-plus'></i> Thêm mới", "class" : "btn btn-sm btn-primary") }}
                <button type="submit" class="btn btn-sm btn-danger" name="submit">
                    <span class="fa fa-trash-o"></span> Xóa các mục đã chọn
                </button>
            </p>
            {% if papers | length %}
            <table id="papers" class="table table-bordered table-hover data-table">
                <thead>
                    <tr>
                        <th style="min-width: 20px;"><input type="checkbox" id="chkSelectAll"></th>
                        <th>ID</th>
                        <th>Tên</th>
                        <th style="min-width: 60px;">Chủ đề</th>
                        {% if roleId is defined and roleId != 3 %}
                        <th style="min-width: 70px;">Hội viên</th>
                        {% endif %}
                        <th style="min-width: 70px;">Từ khóa</th>
                        <th style="min-width: 115px;">Ngày sáng tác</th>
                        <th style="min-width: 50px;"></th>
                    </tr>
                </thead>
                <tbody>
                    {% for paper in papers %}
                    <?php
                        $keywords = explode(',', $paper->getKeywords());
                    ?>
                    <tr>
                        <td><input type="checkbox" name="chkSelect[]" value="{{ paper.getId() }}"></td>
                        <td>{{ paper.getId() | e }}</td>
                        <td>
                            {{ paper.getName() | e }}
                            {% if paper.getLink() %}
                                <a href="{{ url(paper.getLink(), false) }}" class="btn btn-xs" target="_blank"><i class="fa fa-external-link"></i></a>
                            {% endif %}
                        </td>
                        <td>{{ paper.chude.getName() }}</td>
                        {% if roleId is defined and roleId != 3 %}
                        <td>{{ paper.hoivien.getName() }}</td>
                        {% endif %}
                        <td>
                        {% for keyword in keywords %}
                            <a href="#{{ keyword | trim }}" class="btn btn-xs keywords"><span class="label label-success">{{ keyword | trim }}</span></a>
                        {% endfor %}
                        </td>
                        <td>{{ paper.getDateComposed() | e }}</td>
                        <td>
                            {{ link_to("tacpham/edit/" ~ paper.getId(), "<span class='fa fa-pencil'></span>", "class" : "btn btn-sm btn-default") }}
                            {{ link_to("tacpham/delete/" ~ paper.getId(), "<span class='fa fa-trash-o'></span>", "class" : "btn btn-sm btn-danger") }}
                        </td>
                    </tr>
                    {% endfor %}
                </tbody>
            </table>
            {% endif %}
        </form>

        <script>
            var chkSelectAll = document.getElementById('chkSelectAll');
            var chkSelects = document.getElementsByName('chkSelect[]');
            chkSelectAll.addEventListener('click', function () {
                for (var i = 0; i < chkSelects.length; i++) {
                    chkSelects[i].checked = this.checked;
                };
            });
            var deleteSelected = document.getElementById('deleteSelected');
            deleteSelected.addEventListener("submit", function (e) {
                if (!confirm("Bạn có chắc chắn?")) {
                    e.preventDefault();
                    return false;
                }
            });
            $('.btn-danger').on('click', function (e) {
                if (!confirm("Bạn có chắc chắn?")) {
                    e.preventDefault();
                    return false;
                }
            });
            $('.keywords').on('click', function (e) {
                e.preventDefault();
                $filter_box = $('#papers_filter').find('input');
                $filter_box.val($(this).attr('href').slice(1));
                $filter_box.keyup();
                $filter_box.focus();
                return false;
            })
        </script>
    </div>
</div>
