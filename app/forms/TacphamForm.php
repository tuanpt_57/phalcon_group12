<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Radio;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Textarea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Mvc\Model\Validator\Numericality;
use Phalcon\Validation\Validator\Url as UrlValidator;

class TacphamForm extends FormBase
{

    public function initialize($entity = null, $options = array())
    {

        if (isset($options['edit'])) {
            $element = new Text("id", array("readonly" => "readonly", "class" => "form-control"));
            $element->setLabel("ID");
            $this->add($element);
        }

        $name = new Text("name", array("required" => "", "autofocus" => "", "class" => "form-control"));
        $name->setLabel("Tên tác phẩm");
        $name->setFilters(array('striptags', 'string'));
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Bạn chưa nhập tên!'
            ))
        ));
        $this->add($name);

        $link = new Text("link", array("class" => "form-control"));
        $link->setLabel("Liên kết");
        $link->setFilters(array('striptags', 'string'));
        $link->addValidators(array(
            new UrlValidator(array(
                'message' => 'Liên kết không hợp lệ!',
                'allowEmpty' => true
            ))
        ));
        $this->add($link);

        $keywords = new Text("keywords", array(
            "class" => "form-control",
            "data-role" => "tagsinput"
        ));
        $keywords->setLabel("Từ khóa");
        $keywords->setFilters(array('striptags', 'string'));
        $this->add($keywords);

        $chudeId = new Select("chudeId", Chude::find(),
            array(
                'using' => array
                (
                    'id',
                    'name'
                ),
                'class' => 'form-control select'
            )
        );
        $chudeId->setLabel("Chủ đề");
        $chudeId->setDefault('');
        $this->add($chudeId);

        if (isset($options['forAdmin'])) {
            $hoivienId = new Select("hoivienId", Hoivien::find(),
                array(
                    'using' => array
                    (
                        'id',
                        'name'
                    ),
                    'class' => 'form-control select2'
                )
            );
            $hoivienId->setLabel("Hội viên");
            $hoivienId->setDefault('');
            $this->add($hoivienId);
        }

        $dateComposed = new Text("dateComposed", array("required" => "", "class" => "form-control datepicker"));
        $dateComposed->setLabel("Ngày sáng tác");
        $dateComposed->setFilters(array('striptags', 'string'));
        $dateComposed->addValidators(array(
            new PresenceOf(array(
                'message' => 'Bạn chưa nhập ngày!'
            ))
        ));
        $this->add($dateComposed);

        $info = new Textarea("info", array(
            "class" => "form-control",
            "rows"  => "5"
        ));
        $info->setLabel("Thông tin thêm");
        $info->setFilters(array('striptags', 'string'));
        $this->add($info);
    }
}
