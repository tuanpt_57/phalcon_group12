<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Textarea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Mvc\Model\Validator\Numericality;

class RoleForm extends FormBase
{

    public function initialize($entity = null, $options = array())
    {

        if (isset($options['edit'])) {
            $element = new Text("id", array("readonly" => "readonly", "class" => "form-control"));
            $element->setLabel("ID");
            $this->add($element);
        }

        $name = new Text("name", array("required" => "", "class" => "form-control"));
        $name->setLabel("Tên vai trò");
        $name->setFilters(array('striptags', 'string'));
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Bạn chưa nhập tên!'
            ))
        ));
        $this->add($name);

        $info = new Textarea("info", array("class" => "form-control", "required" => ""));
        $info->setLabel("Mô tả");
        $info->setFilters(array('striptags', 'string'));
        $info->addValidators(array(
            new PresenceOf(array(
                'message' => 'Bạn chưa nhập mô tả'
            ))
        ));
        $this->add($info);
    }
}
