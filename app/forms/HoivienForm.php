<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Email as EmailElement;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Radio;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Textarea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Mvc\Model\Validator\Numericality;

class HoivienForm extends FormBase
{

    public function initialize($entity = null, $options = array())
    {

        if (isset($options['edit']) && !isset($options['forUser'])) {
            $element = new Text("id", array("readonly" => "readonly", "class" => "form-control"));
            $element->setLabel("ID");
            $this->add($element);
        }

        if (!isset($options['forUser'])) {
            $username = new Text("username", array("required" => "", "autofocus" => "", "class" => "form-control"));
        } else {
            $username = new Text("username", array("readonly" => "readonly", "class" => "form-control"));
        }
        $username->setLabel("Tên đăng nhập");
        $username->setFilters(array('striptags', 'string'));
        $username->addValidators(array(
            new PresenceOf(array(
                'message' => 'Bạn chưa nhập tên!'
            ))
        ));
        $this->add($username);

        $password = new Text("password", array("required" => "", "class" => "form-control"));
        $password->setLabel("Mật khẩu");
        $password->setFilters(array('striptags', 'string'));
        $password->addValidators(array(
            new PresenceOf(array(
                'message' => 'Bạn chưa nhập mật khẩu!'
            ))
        ));
        $this->add($password);

        $name = new Text("name", array("required" => "", "class" => "form-control"));
        $name->setLabel("Họ tên");
        $name->setFilters(array('striptags', 'string'));
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Bạn chưa nhập tên!'
            ))
        ));
        $this->add($name);

        $displayName = new Text("displayName", array("required" => "", "class" => "form-control"));
        $displayName->setLabel("Tên hiển thị");
        $displayName->setFilters(array('striptags', 'string'));
        $displayName->addValidators(array(
            new PresenceOf(array(
                'message' => 'Bạn chưa nhập tên!'
            ))
        ));
        $this->add($displayName);

        $birthday = new Text("birthday", array("required" => "", "class" => "form-control datepicker"));
        $birthday->setLabel("Ngày sinh");
        $birthday->setFilters(array('striptags', 'string'));
        $birthday->addValidators(array(
            new PresenceOf(array(
                'message' => 'Bạn chưa nhập ngày sinh!'
            ))
        ));
        $this->add($birthday);

        $gender = new Select("gender", array(
            'male' => 'Nam',
            'female' => 'Nữ'
            ),
            array(
                'class' => 'form-control'
            )
        );
        $gender->setLabel("Giới tính");
        $this->add($gender);

        $avatar = new File(
            "avatar",
            array(
                'title' => 'Chọn file jpg/png, kích thước < 2MB'
            )
        );
        $avatar->setLabel("Ảnh đại diện");
        $this->add($avatar);

        $country = new Text("country", array("required" => "", "class" => "form-control"));
        $country->setLabel("Quê quán");
        $country->setFilters(array('striptags', 'string'));
        $country->addValidators(array(
            new PresenceOf(array(
                'message' => 'Bạn chưa nhập!'
            ))
        ));
        $this->add($country);

        $email = new EmailElement("email", array("required" => "", "class" => "form-control"));
        $email->setLabel("Email");
        $email->setFilters(array('striptags', 'string'));
        $email->addValidators(array(
            new Email(array(
                'message' => 'Email không hợp lệ!'
            ))
        ));
        $this->add($email);

        $idCard = new Text("idCard", array("required" => "", "class" => "form-control"));
        $idCard->setLabel("Số CMND");
        $idCard->setFilters(array('striptags', 'string'));
        $idCard->addValidators(array(
            new PresenceOf(array(
                'message' => 'Bạn chưa nhập!'
            ))
        ));
        $this->add($idCard);

        if (!isset($options['forUser'])) {
            $roleId = new Select("roleId", Role::find(),
                array(
                    'using' => array
                    (
                        'id',
                        'name'
                    ),
                    'class' => 'form-control'
                )
            );
            $roleId->setLabel("Vai trò");
            $roleId->setDefault('3');
            $this->add($roleId);
        }

        if (!isset($options['forUser'])) {
            $isActive = new Select("isActive", array(
                '1' => 'Kích hoạt',
                '0' => 'Chưa kích hoạt'
                ),
                array(
                    'class' => 'form-control'
                )
            );
            $isActive->setLabel("Trạng thái");
            $this->add($isActive);
        }

        $biography = new Textarea("biography", array(
            "class" => "form-control",
            "required" => "",
            "rows" => "5"
        ));
        $biography->setLabel("Tiểu sử");
        $biography->setFilters(array('striptags', 'string'));
        $biography->addValidators(array(
            new PresenceOf(array(
                'message' => 'Bạn chưa nhập!'
            ))
        ));
        $this->add($biography);
    }
}
