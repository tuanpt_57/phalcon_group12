<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Identical;

class LoginForm extends Form
{

    public function initialize()
    {
        // Email
        $username = new Text('username', array(
            'placeholder' => 'Tên đăng nhập',
            'class' => 'form-control',
            'required' => '',
            "autofocus" => ""
        ));

        $username->addValidators(array(
            new PresenceOf(array(
                'message' => 'Chưa nhập tên đăng nhập!'
            ))
        ));

        $this->add($username);

        // Password
        $password = new Password('password', array(
            'placeholder' => 'Mật khẩu',
            'class' => 'form-control',
            'required' => ''
        ));

        $password->addValidator(new PresenceOf(array(
            'message' => 'Chưa nhập password!'
        )));

        $this->add($password);

        // Remember
        $remember = new Check('remember', array(
            'value' => 'yes'
        ));

        $remember->setLabel('Ghi nhớ đăng nhập');

        $this->add($remember);

    }

    public function renderDecorated($name)
    {
        $element = $this->get($name);
        //Get any generated messages for the current element
        $messages = $this->getMessagesFor($element->getName());
        if (count($messages)) {
            //Print each element
            echo '<div class="messages">';
            foreach ($messages as $message) {
                echo $this->flash->error($message);
            }
            echo '</div>';
        }
        echo '<p>';
        echo '<label for="', $element->getName(), '">', $element->getLabel(), '</label>';
        echo $element;
        echo '</p>';
    }

    public function renderAll()
    {
        $elements = $this->getElements();
        foreach ($elements as $element) {
            echo "<div class='form-group'>";
            echo '<label for="', $element->getName(), '" class="col-sm-2 col-md-3 col-lg-2 control-label">', $element->getLabel(), '</label>';
            echo '<div class="col-sm-10 col-md-9 col-lg-10">';
            echo $element;
            echo '</div>';
            echo "</div>";
        }
        //Get any generated messages for the current element
        $messages = $this->getMessagesFor($element->getName());
        if (count($messages)) {
            //Print each element
            echo '<div class="messages">';
            foreach ($messages as $message) {
                echo $this->flash->error($message);
            }
            echo '</div>';
        }

    }
}
