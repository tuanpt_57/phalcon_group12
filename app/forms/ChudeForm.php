<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Radio;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Textarea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Mvc\Model\Validator\Numericality;

class ChudeForm extends FormBase
{

    public function initialize($entity = null, $options = array())
    {

        if (isset($options['edit'])) {
            $element = new Text("id", array("readonly" => "readonly", "class" => "form-control"));
            $element->setLabel("ID");
            $this->add($element);
        }

        $name = new Text("name", array("required" => "", "autofocus" => "", "class" => "form-control"));
        $name->setLabel("Tên chủ đề");
        $name->setFilters(array('striptags', 'string'));
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Bạn chưa nhập tên!'
            ))
        ));
        $this->add($name);
    }
}
