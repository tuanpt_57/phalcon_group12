<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Radio;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Textarea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Mvc\Model\Validator\Numericality;
use Phalcon\Validation\Validator\Url as UrlValidator;

class GiaithuongForm extends FormBase
{

    public function initialize($entity = null, $options = array())
    {

        if (isset($options['edit'])) {
            $element = new Text("id", array("readonly" => "readonly", "class" => "form-control"));
            $element->setLabel("ID");
            $this->add($element);
        }

        $loaigiaithuongId = new Select("loaigiaithuongId", Loaigiaithuong::find(),
            array(
                'using' => array
                (
                    'id',
                    'name'
                ),
                'class' => 'form-control select2',
                "autofocus" => ""
            )
        );
        $loaigiaithuongId->setLabel("Loại giải thưởng");
        $loaigiaithuongId->setDefault('');
        $this->add($loaigiaithuongId);

        if (isset($options['forAdmin'])) {
            $hoivienId = new Select("hoivienId", Hoivien::find(),
                array(
                    'using' => array
                    (
                        'id',
                        'name'
                    ),
                    'class' => 'form-control select2'
                )
            );
            $hoivienId->setLabel("Hội viên");
            $hoivienId->setDefault('');
            $this->add($hoivienId);
        }

        $dateRewarded = new Text("dateRewarded", array("required" => "", "class" => "form-control datepicker"));
        $dateRewarded->setLabel("Ngày được trao");
        $dateRewarded->setFilters(array('striptags', 'string'));
        $dateRewarded->addValidators(array(
            new PresenceOf(array(
                'message' => 'Bạn chưa nhập ngày!'
            ))
        ));
        $this->add($dateRewarded);

    }
}
