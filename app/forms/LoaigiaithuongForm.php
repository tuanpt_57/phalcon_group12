<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Radio;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Textarea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Mvc\Model\Validator\Numericality;
use Phalcon\Validation\Validator\Url as UrlValidator;

class LoaigiaithuongForm extends FormBase
{

    public function initialize($entity = null, $options = array())
    {

        if (isset($options['edit'])) {
            $element = new Text("id", array("readonly" => "readonly", "class" => "form-control"));
            $element->setLabel("ID");
            $this->add($element);
        }

        $name = new Text("name", array("required" => "", "autofocus" => "", "class" => "form-control"));
        $name->setLabel("Tên giải thưởng");
        $name->setFilters(array('striptags', 'string'));
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Bạn chưa nhập tên!'
            ))
        ));
        $this->add($name);

        $info = new Textarea("info", array("required" => "", "class" => "form-control", "rows" => "8"));
        $info->setLabel("Thông tin");
        $info->setFilters(array('striptags', 'string'));
        $info->addValidators(array(
            new PresenceOf(array(
                'message' => 'Bạn chưa nhập thông tin mô tả!'
            ))
        ));
        $this->add($info);

    }
}
