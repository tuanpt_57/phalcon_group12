<?php

use Phalcon\Forms\Form;

/**
 * Form cơ sở cho tất cả các form trong ứng dụng
*/
class FormBase extends Form
{

    public function renderDecorated($name)
    {
        $element = $this->get($name);
        //Get any generated messages for the current element
        $messages = $this->getMessagesFor($element->getName());
        if (count($messages)) {
            //Print each element
            echo '<div class="messages">';
            foreach ($messages as $message) {
                echo $this->flash->error($message);
            }
            echo '</div>';
        }
        echo '<p>';
        echo '<label for="', $element->getName(), '">', $element->getLabel(), '</label>';
        echo $element;
        echo '</p>';
    }

    public function renderAll()
    {
        $elements = $this->getElements();
        foreach ($elements as $element) {
            echo "<div class='form-group'>";
            echo '<label for="', $element->getName(), '" class="col-sm-2 col-md-3 col-lg-2 control-label">', $element->getLabel(), '</label>';
            echo '<div class="col-sm-10 col-md-9 col-lg-10">';
            echo $element;
            echo '</div>';
            echo "</div>";
        }
        //Get any generated messages for the current element
        $messages = $this->getMessagesFor($element->getName());
        if (count($messages)) {
            //Print each element
            echo '<div class="messages">';
            foreach ($messages as $message) {
                echo $this->flash->error($message);
            }
            echo '</div>';
        }

    }
}
