<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

class GiaithuongMigration_100 extends Migration
{

    public function up()
    {
        $this->morphTable(
            'giaithuong',
            array(
            'columns' => array(
                new Column(
                    'id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'autoIncrement' => true,
                        'size' => 11,
                        'first' => true
                    )
                ),
                new Column(
                    'loaigiaithuong_id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'size' => 11,
                        'after' => 'id'
                    )
                ),
                new Column(
                    'hoivien_id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'size' => 11,
                        'after' => 'loaigiaithuong_id'
                    )
                ),
                new Column(
                    'date_rewarded',
                    array(
                        'type' => Column::TYPE_DATE,
                        'notNull' => true,
                        'size' => 1,
                        'after' => 'hoivien_id'
                    )
                )
            ),
            'indexes' => array(
                new Index('PRIMARY', array('id')),
                new Index('fk_giaithuong_hoivien', array('hoivien_id')),
                new Index('fk_giaithuong_loaigiaithuong', array('loaigiaithuong_id'))
            ),
            'references' => array(
                new Reference('fk_giaithuong_hoivien', array(
                    'referencedSchema' => 'hoinhabao',
                    'referencedTable' => 'hoivien',
                    'columns' => array('hoivien_id'),
                    'referencedColumns' => array('id')
                )),
                new Reference('fk_giaithuong_loaigiaithuong', array(
                    'referencedSchema' => 'hoinhabao',
                    'referencedTable' => 'loaigiaithuong',
                    'columns' => array('loaigiaithuong_id'),
                    'referencedColumns' => array('id')
                ))
            ),
            'options' => array(
                'TABLE_TYPE' => 'BASE TABLE',
                'AUTO_INCREMENT' => '17',
                'ENGINE' => 'InnoDB',
                'TABLE_COLLATION' => 'utf8_unicode_ci'
            )
        )
        );
    }
}
