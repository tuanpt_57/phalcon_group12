<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

class TacphamMigration_200 extends Migration
{

    public function up()
    {
        $this->morphTable(
            'tacpham',
            array(
            'columns' => array(
                new Column(
                    'id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'autoIncrement' => true,
                        'size' => 11,
                        'first' => true
                    )
                ),
                new Column(
                    'name',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'notNull' => true,
                        'size' => 255,
                        'after' => 'id'
                    )
                ),
                new Column(
                    'hoivien_id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'size' => 11,
                        'after' => 'name'
                    )
                ),
                new Column(
                    'chude_id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'size' => 11,
                        'after' => 'hoivien_id'
                    )
                ),
                new Column(
                    'keywords',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'size' => 255,
                        'after' => 'chude_id'
                    )
                ),
                new Column(
                    'date_composed',
                    array(
                        'type' => Column::TYPE_DATE,
                        'notNull' => true,
                        'size' => 1,
                        'after' => 'keywords'
                    )
                ),
                new Column(
                    'link',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'size' => 255,
                        'after' => 'date_composed'
                    )
                ),
                new Column(
                    'added_at',
                    array(
                        'type' => Column::TYPE_DATETIME,
                        'size' => 1,
                        'after' => 'link'
                    )
                ),
                new Column(
                    'info',
                    array(
                        'type' => Column::TYPE_TEXT,
                        'size' => 1,
                        'after' => 'added_at'
                    )
                )
            ),
            'indexes' => array(
                new Index('PRIMARY', array('id')),
                new Index('fk_tacpham_hoivien', array('hoivien_id')),
                new Index('fk_tacpham_chude', array('chude_id'))
            ),
            'references' => array(
                new Reference('fk_tacpham_chude', array(
                    'referencedSchema' => 'hoinhabao',
                    'referencedTable' => 'chude',
                    'columns' => array('chude_id'),
                    'referencedColumns' => array('id')
                )),
                new Reference('fk_tacpham_hoivien', array(
                    'referencedSchema' => 'hoinhabao',
                    'referencedTable' => 'hoivien',
                    'columns' => array('hoivien_id'),
                    'referencedColumns' => array('id')
                ))
            ),
            'options' => array(
                'TABLE_TYPE' => 'BASE TABLE',
                'AUTO_INCREMENT' => '27',
                'ENGINE' => 'InnoDB',
                'TABLE_COLLATION' => 'utf8_unicode_ci'
            )
        )
        );
    }
}
