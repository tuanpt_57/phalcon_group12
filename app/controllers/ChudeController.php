<?php

class ChudeController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
        $this->tag->prependTitle('Chủ đề - ');
        $this->view->setVar('pageHeader', "Quản lý chủ đề");
        $this->view->setVar('breadCrumb', "Chủ đề / Danh sách");
    }

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->view->setVar("subjects", Chude::find());
    }

    public function countTacpham($id)
    {
        return Tacpham::count("chude_id = $id");
    }

    /**
     * Hiển thị fomr nhập mới chủ đề
     */
    public function addAction()
    {
        $this->tag->prependTitle("Thêm ");
        $this->view->setVar('breadCrumb', "Chủ đề / Danh sách / Thêm mới");
        $this->view->form = new ChudeForm();
    }

    /**
     * Lưu chủ đề mới
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->response->redirect('chude');
        }

        $form = new ChudeForm;
        $this->view->form = $form;
        $chude = new Chude();

        $data = $this->request->getPost();
        if (!$form->isValid($data, $chude)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('chude/add');
        }

        if (!$chude->save()) {
            foreach ($chude->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->dispatcher->forward(array(
                'controller' => 'chude',
                'action'     => 'add'
            ));
        }

        $form->clear();

        $this->flash->success("Thêm mới thành công!");

        return $this->response->redirect('chude');
    }

    /**
     * Hiển thị form sửa chủ đề theo ID
     */
    public function editAction($id)
    {
        $this->view->setVar('breadCrumb', "Chủ đề / Danh sách / Sửa");
        if (!$this->request->isPost()) {

            $chude = Chude::findFirstById($id);
            if (!$chude) {
                $this->flash->error("Không tìm thấy chủ đề!");
                return $this->response->redirect("chude");
            }

            $this->view->form = new ChudeForm($chude, array('edit' => true));
        }
    }

    /**
     * Cập nhật chủ đề
     */
    public function saveAction()
    {
        if (!$this->request->isPost()) {
            return $this->response->redirect('chude');
        }

        $id = $this->request->getPost("id", "int");

        $chude = Chude::findFirstById($id);
        if (!$chude) {
            $this->flash->error("Không tìm thấy chủ đề!");
            return $this->response->redirect("chude");
        }

        $form = new ChudeForm($chude, array('edit' => true));
        $this->view->form = $form;

        $data = $this->request->getPost();
        if (!$form->isValid($data, $chude)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('chude/edit/' . $id);
        }

        $chude->assign(array(
            'date_composed' => date('Y-m-d', time())
        ));

        if (!$chude->save()) {
            foreach ($chude->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('chude/edit/' . $id);
        }

        $form->clear();

        $this->flash->success("Cập nhật thành công!");

        return $this->response->redirect('chude');
    }

    /**
     * Xóa một hoặc nhiều chủ đề theo ID
     */
    public function deleteAction($id = "")
    {
        if ($id) {
            $chude = Chude::findFirstById($id);
            if (!$chude) {
                $this->flash->error("Không tìm thấy chủ đề $id!");
                return $this->response->redirect('chude');
            }
            if (!$chude->delete()) {
                $this->flash->error("Không tìm xóa được chủ đề " . $chude->getName() . "!");
                return $this->response->redirect('chude');
            }

            $this->flash->success("Xóa thành công!" . $id);
            return $this->response->redirect('chude');
        } else if (($ids = $this->request->getPost('chkSelect'))) {
            $ids = implode($ids, ',');
            $deletes = Chude::find(array(
                "conditions" => "find_in_set (id, ?1)",
                "bind"       => array(1 => $ids),
                "bindType"   => array(Phalcon\Db\Column::BIND_PARAM_STR)
            ));
            $success = array();
            $error = array();
            foreach ($deletes as $chude) {
                if (!$chude) {
                    $error .= "Không tìm thấy chủ đề $id!\n";
                }
                if ($chude->delete() == false) {
                    $error[] = $chude->name;
                } else {
                    $success[] = $chude->name;
                }
            }
            if ($error) {
                $error = "Không xóa được chủ đề " . implode($error, ", ");
                $this->flash->error($error);
            }
            if ($success) {
                $success = "Xóa thành công chủ đề " . implode($success, ", ");
                $this->flash->success($success);
            }
            return $this->response->redirect('chude');
        } else {
            return $this->response->redirect('chude');
        }
    }

}

