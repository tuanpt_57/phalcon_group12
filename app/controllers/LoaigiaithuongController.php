<?php

class LoaigiaithuongController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
        $this->tag->prependTitle('Danh mục loại giải thưởng');
        $this->view->setVar('pageHeader', "Quản lý loại giải thưởng");
        $this->view->setVar('breadCrumb', "Loại giải thưởng / Danh sách");
    }

    public function indexAction()
    {
        $this->view->setVar("types", Loaigiaithuong::find());
    }

    public function countGiaithuong($id)
    {
        return Giaithuong::count("loaigiaithuong_id = $id");
    }

    public function addAction()
    {
        $this->tag->prependTitle("Thêm mới | ");
        $this->view->setVar('breadCrumb', "Loại giải thưởng / Danh sách / Thêm mới");
        $this->view->form = new LoaigiaithuongForm();
    }

    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->response->redirect('loaigiaithuong');
        }

        $form = new LoaigiaithuongForm;
        $this->view->form = $form;
        $loaigiaithuong = new Loaigiaithuong();

        $data = $this->request->getPost();
        if (!$form->isValid($data, $loaigiaithuong)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('loaigiaithuong/add');
        }

        if (!$loaigiaithuong->save()) {
            foreach ($loaigiaithuong->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('loaigiaithuong/add');
        }

        $form->clear();

        $this->flash->success("Thêm mới thành công!");

        return $this->response->redirect('loaigiaithuong');
    }

    public function editAction($id)
    {
        $this->view->setVar('breadCrumb', "Loại giải thưởng / Danh sách / Sửa");
        if (!$this->request->isPost()) {

            $loaigiaithuong = Loaigiaithuong::findFirstById($id);
            if (!$loaigiaithuong) {
                $this->flash->error("Không tìm thấy loại giải thưởng!");
                return $this->forward("loaigiaithuong/index");
            }

            $this->view->form = new LoaigiaithuongForm($loaigiaithuong, array('edit' => true));
        }
    }

    public function saveAction()
    {
        if (!$this->request->isPost()) {
            return $this->response->redirect('loaigiaithuong');
        }

        $id = $this->request->getPost("id", "int");

        $loaigiaithuong = Loaigiaithuong::findFirstById($id);
        if (!$loaigiaithuong) {
            $this->flash->error("Không tìm thấy loại giải thưởng!");
            return $this->response->redirect('loaigiaithuong');
        }

        $form = new LoaigiaithuongForm($loaigiaithuong, array('edit' => true));
        $this->view->form = $form;

        $data = $this->request->getPost();
        if (!$form->isValid($data, $loaigiaithuong)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('loaigiaithuong/edit/' . $id);
        }

        if (!$loaigiaithuong->save()) {
            foreach ($loaigiaithuong->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('loaigiaithuong/edit/' . $id);
        }

        $form->clear();

        $this->flash->success("Cập nhật thành công!");

        return $this->response->redirect('loaigiaithuong');
    }

    public function deleteAction($id = "")
    {
        if ($id) {
            $loaigiaithuong = Loaigiaithuong::findFirstById($id);
            if (!$loaigiaithuong) {
                $this->flash->error("Không tìm thấy loại giải thưởng $id!");
                return $this->response->redirect('loaigiaithuong');
            }
            if (!$loaigiaithuong->delete()) {
                $this->flash->error("Không tìm xóa được loại giải thưởng " . $loaigiaithuong->getName() . "!");
                return $this->response->redirect('loaigiaithuong');
            }

            $this->flash->success("Xóa thành công!" . $id);
            return $this->response->redirect('loaigiaithuong');
        } else if (($ids = $this->request->getPost('chkSelect'))) {
            $ids = implode($ids, ',');
            $deletes = Loaigiaithuong::find(array(
                "conditions" => "find_in_set (id, ?1)",
                "bind"       => array(1 => $ids),
                "bindType"   => array(Phalcon\Db\Column::BIND_PARAM_STR)
            ));
            $success = array();
            $error = array();
            foreach ($deletes as $loaigiaithuong) {
                if (!$loaigiaithuong) {
                    $error .= "Không tìm thấy loại giải thưởng $id!\n";
                }
                if ($loaigiaithuong->delete() == false) {
                    $error[] = $loaigiaithuong->name;
                } else {
                    $success[] = $loaigiaithuong->name;
                }
            }
            if ($error) {
                $error = "Không xóa được loại giải thưởng " . implode($error, ", ");
                $this->flash->error($error);
            }
            if ($success) {
                $success = "Xóa thành công loại giải thưởng " . implode($success, ", ");
                $this->flash->success($success);
            }
            return $this->response->redirect('loaigiaithuong');
        } else {
            return $this->response->redirect('loaigiaithuong');
        }
    }

}

