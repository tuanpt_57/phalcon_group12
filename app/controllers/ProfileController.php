<?php

use Phalcon\Exception as AuthException;

class ProfileController extends ControllerBase
{

    private $profile;

    public function initialize()
    {
        parent::initialize();
        if (is_array($this->identity)) {
            $roleId = $this->identity['profile'];
            $userId = $this->identity['id'];

            $this->profile = Hoivien::findFirstById($userId);

            $this->view->setVar("profile", $this->profile);
        }
    }

    public function indexAction($username = null)
    {
        if ($username) {
            $this->profile = Hoivien::findFirstByUsername($username);
        }
        if (isset($this->profile) && $this->profile) {
            $this->tag->prependTitle($this->profile->getName() . ' - ');
            $this->view->setVar("profile", $this->profile);
            $this->view->setVar("papers", Tacpham::find("hoivien_id = " . $this->profile->getId()));
            $sql = "select gt.*
                    from Giaithuong gt
                    join Loaigiaithuong lgt
                    join Hoivien hv
                    where gt.hoivien_id = " . $this->profile->getId();
            $prizes = $this->modelsManager->executeQuery($sql);
            $this->view->setVar("prizes", $prizes);
        } else {
            return $this->response->redirect(".");
        }
    }

    public function editAction()
    {
        $id = $this->identity['id'];
        $this->view->setVar('pageHeader', "Sửa thông tin");
        $this->view->setVar('breadCrumb', NULL);
        if (!$this->request->isPost()) {

            $hoivien = Hoivien::findFirstById($id);
            if (!$hoivien) {
                return $this->response->redirect("profile");
            }

            $this->view->form = new HoivienForm($hoivien, array('edit' => true, 'forUser' => true));
        }
    }

    public function saveAction()
    {
        if (!$this->request->isPost()) {
            return $this->response->redirect('profile');
        }

        $id = $this->identity['id'];

        $hoivien = Hoivien::findFirstById($id);
        if (!$hoivien) {
            return $this->response->redirect("profile");
        }

        $form = new HoivienForm($hoivien, array('edit' => true, 'forUser' => true));
        $this->view->form = $form;

        $data = $this->request->getPost();
        if (!$form->isValid($data, $hoivien)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('profile/edit/' . $id);
        }

        // Prevent change Username
        $hoivien->setUsername($this->profile->getUserName());

        // Upload image
        if ($this->request->hasFiles() && $this->request->getUploadedFiles()[0]->getName()) {
            $allowedTypes = [
                'image/jpg',
                'image/png',
                'image/jpeg'
            ];
            $baseLocation = APP_PATH . 'public/assets/uploads/avatar/';
            $username = $this->request->getPost('username', 'striptags');

            $file = $this->request->getUploadedFiles()[0];
            if (!in_array($file->getRealType(), $allowedTypes)) {
                $this->flash->error("Hình ảnh không hợp lệ! Vui lòng chọn ảnh png hoặc jpg.");
                return $this->forward('profile/edit');
            }
            if ($file->getSize() > 2097152) { // 2MiB
                $this->flash->error("Vui lòng chọn ảnh kích thước < 2MB.");
                return $this->forward('profile/edit');
            }

            $filename = $username . "." . str_replace('image/', '', $file->getRealType());
            // Di chuyển file vào thư mục
            $file->moveTo($baseLocation . $filename);

            $hoivien->setAvatar('assets/uploads/avatar/' . $filename);
        }

        $hoivien->assign(array(
            'created_on' => date('Y-m-d H:i:s', time()),
            'ui_template' => 1
        ));

        if (!$hoivien->save()) {
            foreach ($hoivien->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('profile/edit' . $id);
        }

        $form->clear();

        $this->flash->success("Cập nhật thành công!");

        return $this->response->redirect('profile');
    }

    public function loginAction()
    {
        if (!is_array($this->identity)) {

            $this->view->setVar('pageHeader', "Đăng nhập");
            $this->tag->prependTitle('Đăng nhập - ');
            $login_form = new LoginForm;

            try {

                if (!$this->request->isPost()) {
                    if ($this->auth->hasRememberMe()) {
                        return $this->auth->loginWithRememberMe();
                    }
                } else {

                    if ($login_form->isValid($this->request->getPost()) == false) {
                        foreach ($login_form->getMessages() as $message) {
                            $this->flash->error($message);
                        }
                    } else {

                        $check = $this->auth->check(array(
                            'username' => $this->request->getPost('username'),
                            'password' => $this->request->getPost('password'),
                            'remember' => $this->request->getPost('remember')
                        ));
                        if ($check) {
                            return $this->response->redirect('.');
                        }
                    }
                }
            } catch (AuthException $e) {
                $this->flash->error($e->getMessage());
            }
        } else {
            return $this->response->redirect('.');
        }
    }

    public function logoutAction()
    {
        $this->auth->remove();
        return $this->response->redirect('.');
    }

}

