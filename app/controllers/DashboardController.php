<?php

class DashboardController extends ControllerBase
{
    public function indexAction()
    {
        parent::initialize();
        $this->view->setVar("totalHoivien", Hoivien::count());
        $this->view->setVar("totalTacpham", Tacpham::count());
        $this->view->setVar("totalChude", Chude::count());
        $this->view->setVar("members", Hoivien::find(array("limit" => 8, "order" => "id desc")));
        $this->view->setVar("papers", Tacpham::find(array("limit" => 8, "order" => "id desc")));
        $this->view->setVar('pageHeader', "Tổng quan hoạt động");
        $this->view->setVar('breadCrumb', "Dashboard");
        $this->view->logs = Logs::find(array("limit" => 8, "order" => "id desc"));
    }
}

