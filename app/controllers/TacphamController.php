<?php

class TacphamController extends ControllerBase
{

    private $roleId = null;
    private $userId = null;

    public function initialize()
    {
        parent::initialize();
        $this->tag->prependTitle('Tác phẩm - ');
        $this->view->setVar('pageHeader', "Quản lý tác phẩm");
        $this->view->setVar('breadCrumb', "Tác phẩm / Danh sách");
        if (is_array($this->identity)) {
            $this->roleId = $this->identity['profile'];
            $this->userId = $this->identity['id'];
        }
    }

    public function indexAction()
    {
        $sql = "select tp.*
                    from Tacpham tp
                    join Chude
                    join Hoivien";
        if ($this->roleId == 3) {
            $sql .= " where tp.hoivien_id = " . $this->userId;
        }
        $papers = $this->modelsManager->executeQuery($sql);
        $this->view->setVar("papers", $papers);
    }

    public function showAction($page = 1) {
        $this->view->setVar('pageHeader', NULL);
        $this->view->setVar('breadCrumb', NULL);
        $tacpham = Tacpham::find();
        $paginator = new MyPaginatorModel(
            array(
                "data"  => $tacpham,
                "limit" => 8,
                "page"  => $page,
                "link"  => "tacpham/show"
            )
        );
        $this->view->papers = $paginator->getPaginate();
        $this->view->paginator = $paginator;
    }

    public function addAction()
    {
        $this->tag->prependTitle("Thêm ");
        $this->view->setVar('breadCrumb', "Tác phẩm / Danh sách / Thêm mới");
        if ($this->roleId == 3) {
            $this->view->form = new TacphamForm();
        } else {
            $this->view->form = new TacphamForm(NULL, array('forAdmin' => true));
        }
    }

    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->response->redirect('tacpham');
        }

        if ($this->roleId == 3) {
            $form = new TacphamForm();
        } else {
            $form = new TacphamForm(NULL, array('forAdmin' => true));
        }
        $this->view->form = $form;
        $tacpham = new Tacpham();

        $data = $this->request->getPost();
        if (!$form->isValid($data, $tacpham)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('tacpham/add');
        }

        if ($this->roleId == 3) {
            $tacpham->setHoivienId($this->userId);
        }
        $tacpham->assign(array(
            'added_at' => date('Y-m-d H:i:s', time())
        ));

        if (!$tacpham->save()) {
            foreach ($tacpham->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('tacpham/add');
        }

        $form->clear();

        $message = "User " . $this->identity['name'] . ' thêm tác phẩm ' . $tacpham->getName();
        $this->saveLog($message);

        $this->flash->success("Thêm mới thành công!");

        return $this->response->redirect('tacpham');
    }

    public function editAction($id)
    {
        $this->tag->prependTitle("Sửa ");
        $this->view->setVar('breadCrumb', "Tác phẩm / Danh sách / Sửa");
        if (!$this->request->isPost()) {
            if ($this->roleId == 3) {
                $tacpham = Tacpham::findFirst("id = $id and hoivien_id = " . $this->userId);
            } else {
                $tacpham = Tacpham::findFirstById($id);
            }
            if (!$tacpham) {
                $this->flash->error("Không tìm thấy tác phẩm!");
                return $this->response->redirect('tacpham');
            }

            if ($this->roleId == 3) {
                $this->view->form = new TacphamForm($tacpham, array('edit' => true));
            } else {
                $this->view->form = new TacphamForm($tacpham, array('edit' => true, 'forAdmin' => true));
            }
        }
    }

    public function saveAction()
    {
        if (!$this->request->isPost()) {
            return $this->response->redirect('tacpham');
        }

        $id = $this->request->getPost("id", "int");

        if ($this->roleId == 3) {
            $tacpham = Tacpham::findFirst("id = $id and hoivien_id = " . $this->userId);
        } else {
            $tacpham = Tacpham::findFirstById($id);
        }
        if (!$tacpham) {
            $this->flash->error("Không tìm thấy tác phẩm!");
            return $this->response->redirect('tacpham');
        }

        if ($this->roleId == 3) {
            $form = new TacphamForm($tacpham, array('edit' => true));
        } else {
            $form = new TacphamForm($tacpham, array('edit' => true, 'forAdmin' => true));
        }
        $this->view->form = $form;

        $data = $this->request->getPost();
        if (!$form->isValid($data, $tacpham)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('tacpham/edit/' . $id);
        }

        if ($this->roleId == 3) {
            $tacpham->setHoivienId($this->userId);
        }
        $tacpham->assign(array(
            'added_at' => date('Y-m-d H:i:s', time())
        ));

        if (!$tacpham->save()) {
            foreach ($tacpham->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('tacpham/edit/' . $id);
        }

        $form->clear();

        $message = "User " . $this->identity['name'] . ' cập nhật tác phẩm ' . $tacpham->getName();
        $this->saveLog($message);

        $this->flash->success("Cập nhật thành công!");

        return $this->response->redirect('tacpham');
    }

    public function deleteAction($id = "")
    {
        if ($id) {
            if ($this->roleId == 3) {
                $tacpham = Tacpham::findFirst("id = $id and hoivien_id = " . $this->userId);
            } else {
                $tacpham = Tacpham::findFirstById($id);
            }
            if (!$tacpham) {
                $this->flash->error("Không tìm thấy tác phẩm $id!");
                return $this->response->redirect('tacpham');
            }
            if (!$tacpham->delete()) {
                $this->flash->error("Không xóa được tác phẩm " . $tacpham->getName() . "!");
                return $this->response->redirect('tacpham');
            }

            $this->flash->success("Xóa thành công tác phẩm $id!");

            $message = "User " . $this->identity['name'] . ' xóa tác phẩm ' . $tacpham->getName();
            $this->saveLog($message);

            return $this->response->redirect('tacpham');
        } else if (($ids = $this->request->getPost('chkSelect'))) {
            $ids = implode($ids, ',');
            if ($this->roleId == 3) {
                $deletes = Tacpham::find(array(
                    "conditions" => "find_in_set (id, ?1) and hoivien_id = " . $this->userId,
                    "bind"       => array(1 => $ids),
                    "bindType"   => array(Phalcon\Db\Column::BIND_PARAM_STR)
                ));
            } else {
                $deletes = Tacpham::find(array(
                    "conditions" => "find_in_set (id, ?1)",
                    "bind"       => array(1 => $ids),
                    "bindType"   => array(Phalcon\Db\Column::BIND_PARAM_STR)
                ));
            }
            $success = array();
            $error = array();
            foreach ($deletes as $tacpham) {
                if (!$tacpham) {
                    $error .= "Không tìm thấy tác phẩm $id!\n";
                }
                if ($tacpham->delete() == false) {
                    $error[] = $tacpham->getId();
                } else {
                    $success[] = $tacpham->getId();
                }
            }
            if ($error) {
                $error = "Không xóa được tác phẩm " . implode($error, ", ");
                $this->flash->error($error);
            }
            if ($success) {
                $success = "Xóa thành công tác phẩm " . implode($success, ", ");
                $this->flash->success($success);
            }

            $message = "User " . $this->identity['name'] . ' xóa các tác phẩm ' . implode($success, ", ");
            $this->saveLog($message);

            return $this->response->redirect('tacpham');
        } else {
            return $this->response->redirect('tacpham');
        }
    }

}

