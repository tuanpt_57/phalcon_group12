<?php

class GiaithuongController extends ControllerBase
{

    private $roleId = null;
    private $userId = null;

    public function initialize()
    {
        parent::initialize();
        $this->tag->prependTitle('Giải thưởng - ');
        $this->view->setVar('pageHeader', "Quản lý giải thưởng");
        $this->view->setVar('breadCrumb', "Giải thưởng / Danh sách");
        if (is_array($this->identity)) {
            $this->roleId = $this->identity['profile'];
            $this->userId = $this->identity['id'];
        }
    }

    public function indexAction()
    {
        $sql = "select gt.*
                    from Giaithuong gt
                    join Loaigiaithuong
                    join Hoivien";
        if ($this->roleId == 3) {
            $sql .= " where gt.hoivien_id = " . $this->userId;
        }
        $prizes = $this->modelsManager->executeQuery($sql);
        $this->view->setVar("prizes", $prizes);
    }

    public function addAction()
    {
        $this->tag->prependTitle("Thêm ");
        $this->view->setVar('breadCrumb', "Giải thưởng / Danh sách / Thêm mới");
        if ($this->roleId == 3) {
            $this->view->form = new GiaithuongForm();
        } else {
            $this->view->form = new GiaithuongForm(NULL, array('forAdmin' => true));
        }
    }

    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->response->redirect('giaithuong');
        }

        if ($this->roleId == 3) {
            $form = new GiaithuongForm();
        } else {
            $form = new GiaithuongForm(NULL, array('forAdmin' => true));
        }
        $this->view->form = $form;
        $giaithuong = new Giaithuong();

        $data = $this->request->getPost();
        if (!$form->isValid($data, $giaithuong)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('giaithuong/add');
        }

        if ($this->roleId == 3) {
            $giaithuong->setHoivienId($this->userId);
        }

        if (!$giaithuong->save()) {
            foreach ($giaithuong->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('giaithuong/add');
        }

        $form->clear();

        $message = "User " . $this->identity['username'] . ' thêm giải thưởng ID ' . $giaithuong->getId();
        $this->saveLog($message);

        $this->flash->success("Thêm mới thành công!" . $dateRewarded);

        return $this->response->redirect('giaithuong');
    }

    public function editAction($id)
    {
        $this->view->setVar('breadCrumb', "Giải thưởng / Danh sách / Sửa");
        if (!$this->request->isPost()) {
            if ($this->roleId == 3) {
                $giaithuong = Giaithuong::findFirst("id = $id and hoivien_id = " . $this->userId);
            } else {
                $giaithuong = Giaithuong::findFirstById($id);
            }
            if (!$giaithuong) {
                $this->flash->error("Không tìm thấy giải thưởng!");
                return $this->response->redirect('giaithuong');
            }

            if ($this->roleId == 3) {
                $form = new GiaithuongForm($giaithuong, array('edit' => true));
            } else {
                $form = new GiaithuongForm($giaithuong, array('edit' => true, 'forAdmin' => true));
            }
            $this->view->form = $form;
        }
    }

    public function saveAction()
    {
        if (!$this->request->isPost()) {
            return $this->response->redirect('giaithuong');
        }

        $id = $this->request->getPost("id", "int");

        if ($this->roleId == 3) {
            $giaithuong = Giaithuong::findFirst("id = $id and hoivien_id = " . $this->userId);
        } else {
            $giaithuong = Giaithuong::findFirstById($id);
        }
        if (!$giaithuong) {
            $this->flash->error("Không tìm thấy giải thưởng!");
            return $this->response->redirect('giaithuong');
        }

        if ($this->roleId == 3) {
            $form = new GiaithuongForm($giaithuong, array('edit' => true));
        } else {
            $form = new GiaithuongForm($giaithuong, array('edit' => true, 'forAdmin' => true));
        }
        $form = new GiaithuongForm($giaithuong, array('edit' => true));
        $this->view->form = $form;

        $data = $this->request->getPost();
        if (!$form->isValid($data, $giaithuong)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('giaithuong/edit/' . $id);
        }

        if ($this->roleId == 3) {
            $giaithuong->setHoivienId($this->userId);
        }

        if (!$giaithuong->save()) {
            foreach ($giaithuong->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('giaithuong/edit/' . $id);
        }

        $form->clear();

        $message = "User " . $this->identity['username'] . ' sửa giải thưởng ' . $giaithuong->getId();
        $this->saveLog($message);
        $this->flash->success("Cập nhật thành công!");

        return $this->response->redirect('giaithuong');
    }

    public function deleteAction($id = "")
    {
        if ($id) {
            if ($this->roleId == 3) {
                $giaithuong = Giaithuong::findFirst("id = $id and hoivien_id = " . $this->userId);
            } else {
                $giaithuong = Giaithuong::findFirstById($id);
            }
            if (!$giaithuong) {
                $this->flash->error("Không tìm thấy giải thưởng $id!");
                return $this->response->redirect('giaithuong');
            }
            if (!$giaithuong->delete()) {
                $this->flash->error("Không xóa được giải thưởng " . $giaithuong->getId() . "!");
                return $this->response->redirect('giaithuong');
            }

            $message = "User " . $this->identity['username'] . ' xóa giải thưởng ' . $giaithuong->getId();
            $this->saveLog($message);

            $this->flash->success("Xóa thành công!");
            return $this->response->redirect('giaithuong');
        } else if (($ids = $this->request->getPost('chkSelect'))) {
            $ids = implode($ids, ',');
            if ($this->roleId == 3) {
                $deletes = Giaithuong::find(array(
                    "conditions" => "find_in_set (id, ?1) and hoivien_id = " . $this->userId,
                    "bind"       => array(1 => $ids),
                    "bindType"   => array(Phalcon\Db\Column::BIND_PARAM_STR)
                ));
            } else {
                $deletes = Giaithuong::find(array(
                    "conditions" => "find_in_set (id, ?1)",
                    "bind"       => array(1 => $ids),
                    "bindType"   => array(Phalcon\Db\Column::BIND_PARAM_STR)
                ));
            }
            $success = array();
            $error = array();
            foreach ($deletes as $giaithuong) {
                if (!$giaithuong) {
                    $error .= "Không tìm thấy giải thưởng $id!\n";
                }
                if ($giaithuong->delete() == false) {
                    $error[] = $giaithuong->id;
                } else {
                    $success[] = $giaithuong->id;
                }
            }
            if ($error) {
                $error = "Không xóa được giải thưởng " . implode($error, ", ");
                $this->flash->error($error);
            }
            if ($success) {
                $success = "Xóa thành công giải thưởng " . implode($success, ", ");
                $this->flash->success($success);
            }
            $message = "User " . $this->identity['username'] . ' xóa các giải thưởng ' . implode($success, ", ");
            $this->saveLog($message);
            return $this->response->redirect('giaithuong');
        } else {
            return $this->response->redirect('giaithuong');
        }
    }

}

