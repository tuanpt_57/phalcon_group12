<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

/**
 * ControllerBase
 * Tất cả controller trong ứng dụng sẽ kết thừa controller này
 */
class ControllerBase extends Controller
{
    protected $identity;

    protected function initialize()
    {
        $this->identity = $this->auth->getIdentity();
        if (is_array($this->identity)) {
            $roleId = $this->identity['profile'];
            if ($roleId == 1) {
                $this->view->setTemplateAfter('admin');
            } elseif ($roleId == 2) {
                $this->view->setTemplateAfter('mod');
            } elseif ($roleId == 3) {
                $this->view->setTemplateAfter('member');
            }
            $this->view->setVar('userName', $this->identity['name']);
            $this->view->setVar('roleId', $roleId);
        } else {
            $this->view->setTemplateAfter('guest');
            if ($this->auth->hasRememberMe()) {
                return $this->auth->loginWithRememberMe();
            }
        }
        $this->tag->setTitle('Hội nhà báo Việt Nam - VJA');

        $this->view->setVar('login_form', new LoginForm());
        $this->view->setVar('identity', $this->identity);
    }

    protected function forward($uri)
    {
        $uriParts = explode('/', $uri);
        $params = array_slice($uriParts, 2);
        return $this->dispatcher->forward(
            array(
                'controller' => $uriParts[0],
                'action' => $uriParts[1],
                'params' => $params
            )
        );
    }

    protected function saveLog($message)
    {
        $logs = new Logs();
        $logs->setInfo($message);
        $logs->setTime(date('Y-m-d H:i:s', time()));
        $logs->save();
    }

    public function searchAction($search = "") {
        if (!$search) {
            $search = $this->request->get('search', 'string');
        }
        $this->view->pick("shared/search");
        $this->view->setVar("search", $search);
        $this->view->setVar('pageHeader', "Kết quả tìm kiếm <q>$search</q>");
        $search = $search . '%';
        $this->view->setVar("members", Hoivien::find(array(
            "conditions" => "name like ?1 or username like ?1",
            "bind" => array(1 => $search)
        )));
        $this->view->setVar("papers", Tacpham::find(array(
            "conditions" => "name like ?1",
            "bind" => array(1 => $search)
        )));
    }
}
