<?php

class IndexController extends ControllerBase
{
    public function indexAction()
    {
        parent::initialize();
        if (!$this->session->theme) {
            $this->session->theme = 1;
        }
        $theme = $this->session->theme;
        if ($theme && $theme != 1) {
            $this->view->pick('index/index' . $theme);
        }
        $this->view->setVar("totalHoivien", Hoivien::count());
        $this->view->setVar("totalTacpham", Tacpham::count());

        $sql = "select hv.name, hv.username, hv.avatar,
                substr(hv.biography, 1, 40) as biography,
                count(*) as num_papers
                    from Tacpham tp
                    join Hoivien hv
                    group by hoivien_id
                    order by num_papers desc";
        if ($theme == 1) {
            $sql .= " limit 3";
        } else {
            $sql .= " limit 4";
        }
        $topMembers = $this->modelsManager->executeQuery($sql);
        $this->view->setVar("topMembers", $topMembers);

        $this->view->setVar("newMembers", Hoivien::find(array("limit" => 3, "order" => "id desc")));
        $this->view->setVar("papers", Tacpham::find(array("limit" => 8, "order" => "id desc")));
    }

    public function setThemeAction($id = 1)
    {
        $this->session->theme = $id;
        $this->view->disable();
        $this->response->redirect(".");
    }

}

