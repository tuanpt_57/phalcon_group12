<?php

class HoivienController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
        $this->tag->prependTitle('Hội viên - ');
        $this->view->setVar('pageHeader', "Quản lý hội viên");
        $this->view->setVar('breadCrumb', "Hội viên / Danh sách");
    }

    public function countTacpham($id)
    {
        return Tacpham::count("hoivien_id = $id");
    }

    public function countGiaithuong($id)
    {
        return Giaithuong::count("hoivien_id = $id");
    }

    public function indexAction()
    {
        $this->view->setVar("members", Hoivien::find());
    }

    public function listadminAction()
    {
        $this->tag->prependTitle("Danh sách quản trị - ");
        $this->view->setVar("members", Hoivien::find(
            array(
                "conditions" => "role_id = 1 or role_id = 2"
            )
        ));
        $this->view->setVar('breadCrumb', "Hội viên / Danh sách quản trị viên");
    }

    public function showAction($page = 1) {
        $this->view->setVar('pageHeader', NULL);
        $this->view->setVar('breadCrumb', NULL);
        $hoivien = Hoivien::find();
        $paginator = new MyPaginatorModel(
            array(
                "data"  => $hoivien,
                "limit" => 8,
                "page"  => $page,
                "link"  => "hoivien/show"
            )
        );
        $this->view->members = $paginator->getPaginate();
        $this->view->paginator = $paginator;
    }

    public function setActiveAction($id) {
        $hoivien = Hoivien::findFirstById($id);
        $hoivien->setIsActive('1');
        $hoivien->update();
        $this->view->disable();
        $this->response->redirect('hoivien');
    }

    public function setInActiveAction($id) {
        $hoivien = Hoivien::findFirstById($id);
        $hoivien->setIsActive('0');
        $hoivien->update();
        $this->view->disable();
        $this->response->redirect('hoivien');
    }

    public function setModAction($hoivien_id = "")
    {
        if ($hoivien_id) {
            $hoivien = Hoivien::findFirstById($hoivien_id);
            if ($hoivien) {
                $hoivien->setRoleId('2');
                $hoivien->update();
                $this->view->disable();
            } else {
                $this->flash->error("Hội viên không tồn tại!");
            }
        }
        return $this->response->redirect('hoivien/listadmin');
    }

    public function unSetModAction($hoivien_id = "")
    {
        if ($hoivien_id) {
            $hoivien = Hoivien::findFirstById($hoivien_id);
            if ($hoivien) {
                $hoivien->setRoleId('3');
                $hoivien->update();
                $this->view->disable();
            } else {
                $this->flash->error("Hội viên không tồn tại!");
            }
        }
        return $this->response->redirect('hoivien/listadmin');
    }

    public function addAction()
    {
        $this->tag->prependTitle("Thêm ");
        $this->view->setVar('breadCrumb', "Hội viên / Thêm mới");
        $this->view->form = new HoivienForm();
    }

    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->response->redirect('hoivien');
        }

        $form = new HoivienForm;
        $this->view->form = $form;
        $hoivien = new Hoivien();

        $data = $this->request->getPost();
        if (!$form->isValid($data, $hoivien)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('hoivien/add');
        }

        $username = $this->request->getPost('username', 'striptags');
        if (Hoivien::findFirstByUsername($username)) {
            $this->flash->error("Username $username đã tồn tại, vui lòng chọn tên khác!");
            return $this->forward('hoivien/add');
        }

        $email = $this->request->getPost('email', 'striptags');
        if (Hoivien::findFirstByEmail($email)) {
            $this->flash->error("Email $email đã được sử dụng, vui lòng nhập email khác!");
            return $this->forward('hoivien/add');
        }

        $idCard = $this->request->getPost('idCard', 'striptags');
        if (Hoivien::findFirst("id_card = $idCard")) {
            $this->flash->error("Số CMND $idCard đã được sử dụng, vui lòng nhập lại!");
            return $this->forward('hoivien/add');
        }

        // Upload image
        if ($this->request->hasFiles() && $this->request->getUploadedFiles()[0]->getName()) {
            $allowedTypes = [
                'image/jpg',
                'image/png',
                'image/jpeg'
            ];
            $baseLocation = APP_PATH . 'public/assets/uploads/avatar/';

            $file = $this->request->getUploadedFiles()[0];
            if (!in_array($file->getRealType(), $allowedTypes)) {
                $this->flash->error("Hình ảnh không hợp lệ! Vui lòng chọn ảnh png hoặc jpg.");
                return $this->forward('hoivien/add');
            }
            if ($file->getSize() > 2097152) { // 2MiB
                $this->flash->error("Vui lòng chọn ảnh kích thước < 2MB.");
                return $this->forward('hoivien/add');
            }

            $filename .= $username . "." . str_replace('image/', '', $file->getRealType());
            // Di chuyển file vào thư mục
            $file->moveTo($baseLocation . $filename);

            $hoivien->avatar = 'assets/uploads/avatar/' . $filename;
        } else {
            $gender = $this->request->getPost('gender', 'striptags');
            if ($gender == 'male') {
                $hoivien->avatar = 'assets/uploads/avatar/default_male.jpg';
            } else {
                $hoivien->avatar = 'assets/uploads/avatar/default_female.jpg';
            }
        }

        $hoivien->assign(array(
            'created_on' => date('Y-m-d H:i:s', time()),
            'ui_template' => 1
        ));

        if (!$hoivien->save()) {
            foreach ($hoivien->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('hoivien/add');
        }

        $form->clear();

        $this->flash->success("Thêm mới thành công!");

        return $this->response->redirect('hoivien');
    }

    public function editAction($id = "")
    {
        $this->tag->prependTitle("Sửa ");
        $this->view->setVar('breadCrumb', "Hội viên / Sửa");
        if (!$this->request->isPost()) {

            $hoivien = Hoivien::findFirstById($id);
            if (!$hoivien) {
                $this->flash->error("Không tìm thấy hội viên!");
                return $this->response->redirect("hoivien");
            }

            $this->view->form = new HoivienForm($hoivien, array('edit' => true));
        }
    }

    public function saveAction()
    {
        if (!$this->request->isPost()) {
            return $this->response->redirect('hoivien');
        }

        $id = $this->request->getPost("id", "int");

        $hoivien = Hoivien::findFirstById($id);
        if (!$hoivien) {
            $this->flash->error("Không tìm thấy hội viên!");
            return $this->response->redirect("hoivien");
        }

        $form = new HoivienForm($hoivien, array('edit' => true));
        $this->view->form = $form;

        $data = $this->request->getPost();
        if (!$form->isValid($data, $hoivien)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('hoivien/edit/' . $id);
        }

        // Upload image
        if ($this->request->hasFiles() && $this->request->getUploadedFiles()[0]->getName()) {
            $allowedTypes = [
                'image/jpg',
                'image/png',
                'image/jpeg'
            ];
            $baseLocation = APP_PATH . 'public/assets/uploads/avatar/';
            $username = $this->request->getPost('username', 'striptags');

            $file = $this->request->getUploadedFiles()[0];
            if (!in_array($file->getRealType(), $allowedTypes)) {
                $this->flash->error("Hình ảnh không hợp lệ! Vui lòng chọn ảnh png hoặc jpg.");
                return $this->forward('hoivien/add');
            }
            if ($file->getSize() > 2097152) { // 2MiB
                $this->flash->error("Vui lòng chọn ảnh kích thước < 2MB.");
                return $this->forward('hoivien/add');
            }

            $filename = $username . "." . str_replace('image/', '', $file->getRealType());
            // Di chuyển file vào thư mục
            $file->moveTo($baseLocation . $filename);

            $hoivien->avatar = 'assets/uploads/avatar/' . $filename;
        } else {
            $gender = $this->request->getPost('gender', 'striptags');
            if ($gender == 'male') {
                $hoivien->avatar = 'assets/uploads/avatar/default_male.jpg';
            } else {
                $hoivien->avatar = 'assets/uploads/avatar/default_female.jpg';
            }
        }

        $hoivien->assign(array(
            'created_on' => date('Y-m-d H:i:s', time()),
            'ui_template' => 1
        ));

        if (!$hoivien->save()) {
            foreach ($hoivien->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('hoivien/edit' . $id);
        }

        $form->clear();

        $this->flash->success("Cập nhật thành công!");

        return $this->response->redirect('hoivien');
    }

    public function deleteAction($id = "")
    {
        if ($id) {
            $hoivien = Hoivien::findFirstById($id);
            if (!$hoivien) {
                $this->flash->error("Không tìm thấy hội viên $id!");
                return $this->response->redirect('hoivien');
            }
            if (!$hoivien->delete()) {
                $this->flash->error("Không xóa được hội viên " . $hoivien->getName() . "!");
                return $this->response->redirect('hoivien');
            }

            $this->flash->success("Xóa thành công!" . $id);
            return $this->response->redirect('hoivien');
        } else if (($ids = $this->request->getPost('chkSelect'))) {
            $ids = implode($ids, ',');
            $deletes = Hoivien::find(array(
                "conditions" => "find_in_set (id, ?1)",
                "bind"       => array(1 => $ids),
                "bindType"   => array(Phalcon\Db\Column::BIND_PARAM_STR)
            ));
            $success = array();
            $error = array();
            foreach ($deletes as $hoivien) {
                if (!$hoivien) {
                    $error .= "Không tìm thấy hội viên $id!\n";
                }
                if ($hoivien->delete() == false) {
                    $error[] = $hoivien->name;
                } else {
                    $success[] = $hoivien->name;
                }
            }
            if ($error) {
                $error = "Không xóa được hội viên " . implode($error, ", ");
                $this->flash->error($error);
            }
            if ($success) {
                $success = "Xóa thành công hội viên " . implode($success, ", ");
                $this->flash->success($success);
            }
            return $this->response->redirect('hoivien');
        } else {
            return $this->response->redirect('hoivien');
        }
    }

}

