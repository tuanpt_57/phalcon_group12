<?php

class RoleController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
        $this->tag->prependTitle('Vai trò quản trị - ');
        $this->view->setVar('pageHeader', "Quản lý vai trò");
        $this->view->setVar('breadCrumb', "Hội viên / Vai trò");
    }

    public function indexAction()
    {
        $this->view->setVar("roles", Role::find());
        $this->view->form = new RoleForm;
    }

    public function countHoivien($id)
    {
        return Hoivien::count("role_id = $id");
    }

    public function editAction($id)
    {
        $this->tag->prependTitle("Sửa ");
        if (!$this->request->isPost()) {

            $role = Role::findFirstById($id);
            if (!$role) {
                $this->flash->error("Không tìm thấy role");
                return $this->response->redirect("role");
            }

            $this->view->form = new RoleForm($role, array('edit' => true));
        }
    }

    public function saveAction()
    {
        if (!$this->request->isPost()) {
            return $this->response->redirect("role");
        }

        $id = $this->request->getPost("id", "int");

        $role = Role::findFirstById($id);
        if (!$role) {
            $this->flash->error("Role does not exist");
            return $this->response->redirect("role");
        }

        $form = new RoleForm($role, array('edit' => true));
        $this->view->form = $form;

        $data = $this->request->getPost();

        if (!$form->isValid($data, $role)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('roles/edit/' . $id);
        }

        if ($role->save() == false) {
            foreach ($role->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('roles/edit/' . $id);
        }

        $form->clear();

        $this->flash->success("Cập nhật thành công");
        return $this->response->redirect("role");
    }
}

