<?php

use Phalcon\Acl;
use Phalcon\Acl\Role as AclRole;
use Phalcon\Acl\Resource;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Acl\Adapter\Memory as AclList;
use Phalcon\Text;

/**
 * SecurityPlugin
 *
 * Plugin quản lý ACL
 */
class SecurityPlugin extends Plugin
{

	/**
	 * Trả về ACL đã có hoặc tạo mới
	 *
	 * @returns AclList
	 */
	public function getAcl()
	{

		// if (!isset($this->persistent->acl)) {
        $this->persistent->destroy();

			$acl = new AclList();

            // Mặc định là không có quyền gì
			$acl->setDefaultAction(Acl::DENY);

			// Khai báo các roles, ở đây là khai báo theo ID trong Model Role
			$roles = Role::find();
			foreach ($roles as $role) {
				$acl->addRole($role->getId());
			}

			// Quyền của Admin, ID = 1
			$adminResources = array(
                'profile'        => array('edit', 'save'),
                'dashboard'      => array('index'),
		        'hoivien' 	   	 => array('index', 'add', 'edit', 'create', 'save', 'delete', 'listadmin', 'setactive', 'setinactive', 'setmod', 'unsetmod'),
		        'role' 			 => array('index', 'add', 'edit', 'create', 'save', 'delete'),
                'chude'          => array('index', 'add', 'edit', 'create', 'save', 'delete'),
                'giaithuong'     => array('index', 'add', 'edit', 'create', 'save', 'delete'),
                'loaigiaithuong' => array('index', 'add', 'edit', 'create', 'save', 'delete'),
		        'tacpham' 		 => array('index', 'add', 'edit', 'create', 'save', 'delete')
			);

			foreach ($adminResources as $resource => $actions) {
				$acl->addResource(new Resource($resource), $actions);
			}

			// Gán quyền truy cập cho Admin
			foreach ($adminResources as $resource => $actions) {
				foreach ($actions as $action){
					$acl->allow('1', $resource, $action);
				}
			}

			// Quyền của Moderator, ID = 2
			$modResources = array(
                'profile'        => array('edit', 'save'),
		        'hoivien' 	   	 => array('index', 'setactive', 'setinactive'),
		        'role' 			 => array('index'),
                'chude'          => array('index', 'add', 'edit', 'create', 'save', 'delete'),
                'giaithuong'     => array('index', 'add', 'edit', 'create', 'save', 'delete'),
                'tacpham'        => array('index', 'add', 'edit', 'create', 'save', 'delete'),
                'loaigiaithuong' => array('index', 'add', 'edit', 'create', 'save', 'delete')
			);

			foreach ($modResources as $resource => $actions) {
				$acl->addResource(new Resource($resource), $actions);
			}

			// Gán quyền cho Moderators
			foreach ($modResources as $resource => $actions) {
				foreach ($actions as $action){
					$acl->allow('2', $resource, $action);
				}
			}

			// Quyền của Member, ID = 3
			$memberResources = array(
                'profile'        => array('edit', 'save'),
		        'giaithuong'   	 => array('index', 'add', 'edit', 'create', 'save', 'delete'),
		        'tacpham' 		 => array('index', 'add', 'edit', 'create', 'save', 'delete')
			);

			foreach ($memberResources as $resource => $actions) {
				$acl->addResource(new Resource($resource), $actions);
			}

			// Gán quyền truy cập cho Member
			foreach ($memberResources as $resource => $actions) {
				foreach ($actions as $action) {
					$acl->allow('3', $resource, $action);
				}
			}

			// Quyền cho khách, ID = 4
			$publicResources = array(
				'index'      	=> array('index', 'search', 'settheme'),
				'chude'      	=> array('search'),
				'giaithuong'    => array('search'),
				'hoivien'      	=> array('search', 'show'),
				'loaigiaithuong'=> array('search'),
				'role'      	=> array('search'),
                'tacpham'       => array('search', 'show'),
				'profile'      	=> array('index', 'login', 'logout'),
			);
			foreach ($publicResources as $resource => $actions) {
				$acl->addResource(new Resource($resource), $actions);
			}

			// Gán quyền truy cập cho khách
			foreach ($roles as $role) {
				foreach ($publicResources as $resource => $actions) {
					foreach ($actions as $action){
						$acl->allow($role->getId(), $resource, $action);
					}
				}
			}


			// Lưu acl vào cache
		// 	$this->persistent->acl = $acl;
		// }

		return $acl;
        // return $this->persistent->acl;
	}

	/**
	 * Action này chạy trước tất cả các action khác trong ứng dụng
	 *
	 * @param Event $event
	 * @param Dispatcher $dispatcher
	 */
	public function beforeDispatch(Event $event, Dispatcher $dispatcher)
	{
        $dispatcher->setActionName(Text::lower(Text::camelize($dispatcher->getActionName())));
        $dispatcher->setControllerName(Text::lower(Text::camelize($dispatcher->getControllerName())));

		$controllerName = $dispatcher->getControllerName();

        // Check quyền trên các controller riêng tư
        if ($controllerName !== "index") {

            // Lấy định danh hiện tại
            $identity = $this->auth->getIdentity();

            // Nếu chưa có người dùng nào đăng nhập, thì gán định danh là Khách
            if (!is_array($identity)) {
            	$identity = array();

                $identity['profile'] = '4';
            }

            // Kiểm tra quyền truy cập với action đang được người dùng truy cập
            $actionName = $dispatcher->getActionName();

            $acl = $this->getAcl();
            if ($acl->isAllowed($identity['profile'], $controllerName, $actionName) != Acl::ALLOW) {

                $this->flash->notice('Permission denied: ' . $controllerName . '/' . $actionName);

                if ($acl->isAllowed($identity['profile'], $controllerName, 'index')) {
                    $dispatcher->forward(array(
                        'controller' => $controllerName,
                        'action' => 'index'
                    ));
                    // return false;
                    return $this->response->redirect($controllerName);
                } else {
                    $dispatcher->forward(array(
                        'controller' => 'index',
                        'action' => 'index'
                    ));
                    return $this->response->redirect(".");
                }

            }
        }
	}
}
