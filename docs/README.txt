Nhóm 12 - Phát triển ứng dụng web 2014-2015


Công nghệ:
- PHP Phalcon Framework 2.0, MySQL
- jQuery, Bootstrap, jQuery-Datatabases, Font-awesome, Bootstrap Datepicker, Bootstrap Tagsinput, Select2


Cài đặt:
- Database hoinhabao, file database/hoinhabao.sql
- Cài đặt Phalcon: Xem tài liệu trong thư mục docs/
- Thiết lập lại cấu hình trong file app/config/config.php, chú ý phần: database và baseUri


Code: https://bitbucket.org/tuanpt_57/phalcon_group12
Demo: https://vja-nhom12.c9.io/
2 loại tài khoản demo:
- Admin: admin/admin
- Hội viên: member/member


Đề bài: Phân tích thiết kế và xây dựng phần mềm hệ thống quản lý hội viên hội nhà báo Việt Nam
Yêu cầu: Phần mềm cần có các chức năng cơ bản sau:
1. Phân hệ dành cho người quản trị:
a) Cấp tài khoản truy cập hệ thống cho từng hội viên
b) Phân quyền trên chức năng / nhóm chức năng cho từng hội viên
c) Cập nhật (Thêm mới, Sửa thông tin, Xóa bỏ) hồ sơ thông tin từng hội viên
d) Tìm kiếm hội viên theo nhiều tiêu chí
e) Liệt kê được các tác phẩm (bài báo) của hội viên
f) Xây dựng được các báo cáo:
i. Số hội viên tại thời điểm hiện tại
ii. Số lượng và danh sách chi tiết các hội viên theo từng tiêu chí (ví dụ như: tỉnh, tòa báo …)
iii. Thống kê được các tác phẩm, giải thưởng của hội viên iv. v.v..
2. Phân hệ dành cho từng hội viên:
a) Khai báo được thông tin cá nhân của mình (Thông tin cá nhân, cập nhật tác phẩm, giải thưởng)
b) Sửa đổi thông tin cá nhân
3. Phân hệ hiển thị trên website:
a) Cho phép xem thông tin cá nhân dưới dạng: http://tenmien//tenhoivien
b) Xây dựng 03 template cho phép hội viên thiết lập giao diện hiển thị
c) Có liên kết để có thể xem hội viên theo a,b,c và tìm kiếm hội viên để hiển thị.
